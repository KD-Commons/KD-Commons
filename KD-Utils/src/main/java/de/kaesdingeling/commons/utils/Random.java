package de.kaesdingeling.commons.utils;

import java.util.ArrayList;

import org.apache.commons.lang3.RandomStringUtils;

public class Random {
	public ArrayList<String> sqlKeys = new ArrayList<String>();
	
	public static String string(Integer length) {
		return RandomStringUtils.randomAlphanumeric(length);
	}
	
	public static String integer(Integer length) {
		return RandomStringUtils.randomNumeric(length);
	}
	
	public void clearKeys() {
		this.sqlKeys = new ArrayList<String>();
	}
	
	public String generateKey() {
		String key = string(8);
		this.sqlKeys.add(key);
		return key;
	}
	
	public String[] getSqlKeys() {
		String[] array = new String[this.sqlKeys.size()];
		array = this.sqlKeys.toArray(array);
		return array;
	}
	
	public Integer generateTicketID() {
		return Integer.parseInt(integer(8));
	}
}