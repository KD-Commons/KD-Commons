package de.kaesdingeling.commons.utils.interfaces;

@FunctionalInterface
public interface EventListener {
	public void event();
}