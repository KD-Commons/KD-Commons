package de.kaesdingeling.commons.utils.interfaces;

import org.quartz.Job;

public interface ICronRuntime extends Job {
	public void start();
	public void stop();
	public long lastExecute();
}