package de.kaesdingeling.commons.utils.language.interfaces;

import java.util.List;

import de.kaesdingeling.commons.utils.language.LanguageConfig;

/**
 * 
 * @created 14.06.2020 - 13:58:04
 * @author KaesDingeling
 * @version 0.1
 */
public interface ILanguageService {

	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 15:30:52
	 * @author KaesDingeling
	 */
	public List<ILanguageBundle> getBundles();
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 16:24:51
	 * @author KaesDingeling
	 */
	public default ILanguageBundle getDefault() {
		return getBundles().stream().filter(i -> i.isDefaultLocale()).findAny().orElse(null);
	}
	
	/**
	 * 
	 * 
	 * @Created 14.06.2020 - 15:32:26
	 * @author KaesDingeling
	 */
	public default void init() {
		LanguageConfig.languageService = this;
		
		load();
	}
	
	/**
	 * 
	 * 
	 * @Created 14.06.2020 - 15:32:33
	 * @author KaesDingeling
	 */
	public void load();
}