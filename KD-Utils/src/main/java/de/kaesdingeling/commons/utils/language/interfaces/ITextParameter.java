package de.kaesdingeling.commons.utils.language.interfaces;

/**
 * 
 * @created 14.06.2020 - 14:09:09
 * @author KaesDingeling
 * @version 0.1
 */
public interface ITextParameter<I extends ITextParameter<I>> extends Cloneable {
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 14:09:28
	 * @author KaesDingeling
	 */
	public int getPosition();
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 14:09:37
	 * @author KaesDingeling
	 */
	public String getParameter();
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 14:12:50
	 * @author KaesDingeling
	 */
	public I clone();
}