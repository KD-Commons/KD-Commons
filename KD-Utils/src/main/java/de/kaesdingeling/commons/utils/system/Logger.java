package de.kaesdingeling.commons.utils.system;

import java.io.PrintStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import de.kaesdingeling.commons.utils.data.LogLine;
import de.kaesdingeling.commons.utils.data.Parameters;
import de.kaesdingeling.commons.utils.enums.ELogLevel;
import de.kaesdingeling.commons.utils.enums.ELogType;
import de.kaesdingeling.commons.utils.interfaces.ILoggerListener;

public class Logger {
	public static DateFormat dateFormatter = new SimpleDateFormat();
	public static List<LogLine> logs = new ArrayList<LogLine>();
	public static List<ILoggerListener> logListener = new ArrayList<ILoggerListener>();
	
	private final Class<?> _clazz;
	private ELogType logType = null;
	
	public static Logger get(Class<?> _clazz) {
		return new Logger(_clazz);
	}
	
	public static Logger get(Class<?> _clazz, ELogType logType) {
		return new Logger(_clazz).setType(logType);
	}
	
	public Logger(Class<?> _clazz) {
		this._clazz = _clazz;
	}
	
	public static ILoggerListener addChangeListener(ILoggerListener loggerListener) {
		logListener.add(loggerListener);
		return loggerListener;
	}
	
	public static void removeChangeListener(ILoggerListener loggerListener) {
		logListener.remove(loggerListener);
	}
	
	public Logger setType(ELogType logType) {
		this.logType = logType;
		return this;
	}
	
	public static void log(ELogType logType, ELogLevel logLevel, Class<?> _clazz, Object message) {
		if (logType == null) {
			logType = ELogType.OTHER;
		}
		
		LogLine logLine = LogLine.get()
				.setClazz(_clazz)
				.setLogLevel(logLevel)
				.setLogType(logType)
				.setMessage(message)
				.createdNow();
		
		logs.add(logLine);
		
		if (message != null && SystemParameters.get().get(Parameters.LOGGER_SHOW_LOG_LEVELS).contains(logLevel) && SystemParameters.get().get(Parameters.LOGGER_SHOW_LOG_TYPES).contains(logType)) {
			outPut(logLine);
		}
	}
	
	public static void outPut(LogLine logLine) {
		if (ELogLevel.CRITICAL_ERROR.equals(logLine.getLogLevel()) || ELogLevel.ERROR.equals(logLine.getLogLevel()) || ELogLevel.DEBUG.equals(logLine.getLogLevel())) {
			//outPut(logLine, System.err);
		} else {
			outPut(logLine, System.out);
		}
	}
	
	public static void outPut(LogLine logLine, PrintStream stream) {
		String currentTime = dateFormatter.format(logLine.getCreatedDate());
		
		if (logLine.getClazz() != null) {
			stream.println(currentTime + " | " + logLine.getLogType().name() + " >> " + logLine.getClazz().getName());
		}

		stream.println(currentTime + " | " + logLine.getLogType().name() + " >> " + logLine.getMessage());
	}
	
	public void none(ELogType logType, Object message) {
		log(logType, ELogLevel.NONE, _clazz, message);
	}
	
	public void none(Object message) {
		none(logType, message);
	}
	
	public void info(ELogType logType, Object message) {
		log(logType, ELogLevel.INFO, _clazz, message);
	}
	
	public void info(Object message) {
		info(logType, message);
	}
	
	public void warning(ELogType logType, Object message) {
		log(logType, ELogLevel.WARNING, _clazz, message);
	}
	
	public void warning(Object message) {
		none(logType, message);
	}
	
	public void debug(ELogType logType, Object message) {
		log(logType, ELogLevel.DEBUG, _clazz, message);
	}
	
	public void debug(Object message) {
		debug(logType, message);
	}
	
	public void error(ELogType logType, Object message) {
		log(logType, ELogLevel.ERROR, _clazz, message);
	}
	
	public void error(Object message) {
		error(logType, message);
	}
	
	public void critticalError(ELogType logType, Object message) {
		log(logType, ELogLevel.CRITICAL_ERROR, _clazz, message);
	}
	
	public void critticalError(Object message) {
		critticalError(logType, message);
	}
	
	
	
	
}