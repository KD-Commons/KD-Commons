package de.kaesdingeling.commons.utils;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import de.kaesdingeling.commons.utils.enums.ELogType;
import de.kaesdingeling.commons.utils.system.Logger;

public class Password {
	protected static Logger logger = Logger.get(Password.class, ELogType.UTILS);
	
	public static String hash(String passwordToHash, String salt) {
		String generatedPassword = null;
		try {
			MessageDigest md = MessageDigest.getInstance("SHA-512");
			try {
				md.update(salt.getBytes("UTF-8"));
			} catch (UnsupportedEncodingException e) {
				logger.error("password cannot be hash: " + e);
				return null;
			}
			byte[] bytes = null;
			try {
				bytes = md.digest(passwordToHash.getBytes("UTF-8"));
			} catch (UnsupportedEncodingException e) {
				logger.error("password cannot be hash: " + e);
				return null;
			}
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			generatedPassword = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return generatedPassword;
	}
}