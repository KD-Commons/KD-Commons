package de.kaesdingeling.commons.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

public class DateFormats {
	public static DateFormat dateFormat_de_DE = new SimpleDateFormat("dd.MM.yyyy");
	public static DateFormat timeFormat_de_DE = new SimpleDateFormat("HH:mm");
	public static DateFormat timeWithSecoundFormat_de_DE = new SimpleDateFormat("HH:mm:ss");
}