package de.kaesdingeling.commons.utils.database.abstracts;

import java.util.Date;

import javax.persistence.MappedSuperclass;

import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
public abstract class AbstractWithCreatedAndUpdatedEntity extends AbstractWithCreatedEntity {
	private static final long serialVersionUID = -6758398951473522863L;
	
	@Getter
	@Setter
	private long updated;
	
	public void updated() {
		updated = System.currentTimeMillis();
	}
	
	public void setUpdateDate(Date updated) {
		this.updated = updated.getTime();
	}
}