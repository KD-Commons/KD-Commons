package de.kaesdingeling.commons.utils.interfaces;

import de.kaesdingeling.commons.utils.data.LogLine;

public interface ILoggerListener {
	public void newMessage(LogLine logLine);
}