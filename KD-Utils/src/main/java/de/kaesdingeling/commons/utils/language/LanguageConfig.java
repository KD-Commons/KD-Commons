package de.kaesdingeling.commons.utils.language;

import java.util.regex.Pattern;

import de.kaesdingeling.commons.utils.language.interfaces.ILanguageService;

/**
 * 
 * @created 14.06.2020 - 15:57:39
 * @author KaesDingeling
 * @version 0.1
 */
public class LanguageConfig {
	
	public static boolean DEBUG = false;
	
	public static String SEPARATOR = ".";
	public static String DEFAULT_SUFFIX = "label";
	
	public static Pattern REFERENCE_PATTERN = Pattern.compile("@\\{\\w+(?:\\.\\w+)*}@");
	
	public static ILanguageService languageService;
}