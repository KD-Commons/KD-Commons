package de.kaesdingeling.commons.utils.language.models;

import de.kaesdingeling.commons.utils.language.interfaces.ITextParameter;
import lombok.Builder;
import lombok.Data;

/**
 * 
 * @created 14.06.2020 - 14:11:15
 * @author KaesDingeling
 * @version 0.1
 */
@Data
@Builder
public class TextParameter implements ITextParameter<TextParameter> {
	
	private int position;
	private String parameter;
	
	@Override
	public TextParameter clone() {
		return TextParameter.builder()
				.position(position)
				.parameter(parameter)
				.build();
	}
}