package de.kaesdingeling.commons.utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.SharedCacheMode;
import javax.persistence.Table;
import javax.persistence.TypedQuery;
import javax.persistence.ValidationMode;
import javax.persistence.spi.ClassTransformer;
import javax.persistence.spi.PersistenceUnitInfo;
import javax.persistence.spi.PersistenceUnitTransactionType;
import javax.sql.DataSource;

import org.hibernate.jpa.HibernatePersistenceProvider;

import de.kaesdingeling.commons.utils.data.SuccessState;
import de.kaesdingeling.commons.utils.database.abstracts.AbstractEntity;
import de.kaesdingeling.commons.utils.database.abstracts.AbstractWithCreatedAndUpdatedEntity;
import de.kaesdingeling.commons.utils.database.abstracts.AbstractWithCreatedEntity;
import de.kaesdingeling.commons.utils.enums.ELogType;
import de.kaesdingeling.commons.utils.system.DatabaseConfig;
import de.kaesdingeling.commons.utils.system.Logger;

public class JPAConnector {
	protected static Logger logger = Logger.get(JPAConnector.class, ELogType.DATABASE);
	protected Map<String, String> dbConnectionProperties = new HashMap<String, String>();
	protected List<String> databaseEntity = new ArrayList<String>();
	protected ClassLoader classLoader = this.getClass().getClassLoader();
	protected String persistenceUnitName = "KDDBConnector";
	protected EntityManagerFactory emf = null;
	
	public static List<JPAConnector> allDbConnectors = new ArrayList<JPAConnector>();
	public static JPAConnector systemConnector = null;

	public JPAConnector() {
		init();
		connect();
	}
	
	public JPAConnector(boolean connect) {
		init();
		if (connect) {
			connect();
		}
	}
	
	protected void init() {
		allDbConnectors.add(this);
		dbConnectionProperties = DatabaseConfig.get().buildConnection();
		addEntity(AbstractEntity.class);
		addEntity(AbstractWithCreatedEntity.class);
		addEntity(AbstractWithCreatedAndUpdatedEntity.class);
	}
	
	public static JPAConnector getSystem() {
		if (systemConnector == null) {
			systemConnector = new JPAConnector();
		}
		return systemConnector;
	}
	
	public JPAConnector addEntity(Class<?> entityClass) {
		if (entityClass != null) {
			logger.info("add new entity: " + entityClass.getName());
			databaseEntity.add(entityClass.getName());
		}
		return this;
	}

	/**
	 * JPQL Query
	 * <p>
	 * Example: SELECT p FROM CLASS
	 * 
	 * @param query
	 * @param _ReturnType
	 * @return
	 */
	public <T> List<T> findWithQuery(String query, Class<T> _ReturnType) {
		logger.info("query: " + query);
		EntityManager em = getEntityManager();
		if (connected() && !query.isEmpty()) {
			try {
				// Query is the select statement
				TypedQuery<T> q = em.createQuery(query, _ReturnType);
				List<T> result = q.getResultList();
				if (result != null)
					return result;
			} catch (Exception e) {
				logger.error("Failed to find EntityManager: " + e);
			} finally {
				em.close();

			}
		}
		return null;
	}

	/**
	 * Native SQL Query
	 * <p>
	 * Example: SELECT * FROM Table
	 * 
	 * @param query
	 * @return
	 */
	public <T> List<T> findWithSQLQuery(String query, Class<T> _ReturnType) {
		logger.info("query: " + query);
		EntityManager em = getEntityManager();
		if (connected() && !query.isEmpty()) {
			try {
				// Query is the select statement
				Query q = em.createNativeQuery(query, _ReturnType);
				@SuppressWarnings("unchecked")
				List<T> result = q.getResultList();
				if (result != null) {
					return result;
				} else {
					return new ArrayList<T>();
				}
			} catch (Exception e) {
				logger.error("Failed to find EntityManager: " + e);
			} finally {
				em.close();

			}
		}
		return null;
	}
	
	public static String readAllBytes(File file) {
		String sql = "";
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			String line = null;
			while ((line = reader.readLine()) != null) {
				if (sql.equals("")) {
					sql = line;
				} else {
					sql += "\n" + line;
				}
			}
			reader.close();
		} catch (Exception e) {
			
		}
		return sql;
	}
	
	public <T> List<T> findWithSQLQuery(String query) {
		logger.info("query: " + query);
		EntityManager em = getEntityManager();
		if (connected() && !query.isEmpty()) {
			try {
				// Query is the select statement
				Query q = em.createNativeQuery(query);
				@SuppressWarnings("unchecked")
				List<T> result = q.getResultList();
				if (result != null)
					return result;
			} catch (Exception e) {
				logger.error("Failed to find EntityManager: " + e);
			} finally {
				em.close();

			}
		}
		return null;
	}
	
	/**
	 * Native SQL Query
	 * <p>
	 * 
	 * @param _ReturnType
	 * @return
	 */
	public <T> List<T> findAll(Class<T> _ReturnType) {
		return findAll(_ReturnType, null);
	}
	
	/**
	 * Native SQL Query
	 * <p>
	 * Example: WHERE test = 1
	 * 
	 * @param _ReturnType, where
	 * @return
	 */
	public <T> List<T> findAll(Class<T> _ReturnType, String where) {
		EntityManager em = getEntityManager();
		if (connected()) {
			try {
				// Query is the select statement
				Table table = _ReturnType.getAnnotation(Table.class);
				String sql = "SELECT * FROM " + table.name();
				if (where != null && !where.trim().isEmpty()) {
					sql += where;
				}
				Query q = em.createNativeQuery(sql, _ReturnType);
				@SuppressWarnings("unchecked")
				List<T> result = q.getResultList();
				if (result != null)
					return result;
			} catch (Exception e) {
				logger.error("Failed to find EntityManager: " + e);
			} finally {
				em.close();
			}
		}
		return null;
	}
	
	public <T> List<T> find(String query, List<DatabaseValueCache> search, Class<T> _ReturnType) {
		EntityManager em = getEntityManager();
		if (connected() && !query.isEmpty()) {
			try {
				Integer counter = 0;
				String outputQuery = "";
				for (String queryCache : query.split("~")) {
					if (counter > 0) {
						Integer counterCache = counter - 1;
						DatabaseValueCache dataCache = search.get(counterCache);
						outputQuery += ":" + dataCache.getKey();
					}
					outputQuery += queryCache;
					counter++;
				}

				logger.info("query: " + outputQuery);
				
				Query q = em.createNativeQuery(outputQuery, _ReturnType);
				for (DatabaseValueCache databaseValueCache : search) {
					q.setParameter(databaseValueCache.getKey(), databaseValueCache.getValue());
				}
				@SuppressWarnings("unchecked")
				List<T> result = q.getResultList();
				if (result != null)
					return result;
			} catch (Exception e) {
				logger.error("Failed to find EntityManager: " + e);
			} finally {
				em.close();
			}
		}
		return null;
	}
	
	public void connect() {
		if (emf == null || !emf.isOpen()) {
			try {
				emf = new HibernatePersistenceProvider().createContainerEntityManagerFactory(new PersistenceUnitInfo() {
					@Override
					public ValidationMode getValidationMode() {
						return ValidationMode.AUTO;
					}
					
					@Override
					public PersistenceUnitTransactionType getTransactionType() {
						return PersistenceUnitTransactionType.RESOURCE_LOCAL;
					}
					
					@Override
					public SharedCacheMode getSharedCacheMode() {
						return SharedCacheMode.NONE;
					}
					
					@Override
					public Properties getProperties() {
						return null;
					}
					
					@Override
					public String getPersistenceXMLSchemaVersion() {
						return null;
					}
					
					@Override
					public URL getPersistenceUnitRootUrl() {
						return null;
					}
					
					@Override
					public String getPersistenceUnitName() {
						return persistenceUnitName;
					}
					
					@Override
					public String getPersistenceProviderClassName() {
						return "org.hibernate.jpa.HibernatePersistenceProvider";
					}
					
					@Override
					public DataSource getNonJtaDataSource() {
						return null;
					}
					
					@Override
					public ClassLoader getNewTempClassLoader() {
						return classLoader;
					}
					
					@Override
					public List<String> getMappingFileNames() {
						return Collections.emptyList();
					}
					
					@Override
					public List<String> getManagedClassNames() {
						return databaseEntity;
					}
					
					@Override
					public DataSource getJtaDataSource() {
						return null;
					}
					
					@Override
					public List<URL> getJarFileUrls() {
						return Collections.emptyList();
					}
					
					@Override
					public ClassLoader getClassLoader() {
						return classLoader;
					}
					
					@Override
					public boolean excludeUnlistedClasses() {
						return false;
					}
					
					@Override
					public void addTransformer(ClassTransformer transformer) {
					}
				}, dbConnectionProperties);
			} catch (Exception e) {
				logger.error("Error build connection: " + e);
			}
		} else {
			logger.info("already connected");
		}
	}

	public void disconnect() {
		if (emf.isOpen())
			emf.close();
		else
			logger.error("Failed to close database, because it is not connected.");
	}

	public boolean connected() {
		if (emf == null) {
			connect();
		}
		return emf.isOpen();
	}

	private EntityManager getEntityManager() {
		if (connected()) {
			return emf.createEntityManager();
		} else {
			connect();
			return getEntityManager();
		}
	}

	public <T> T findData(Object object, Class<T> _ReturnType) {
		EntityManager em = getEntityManager();
		if (connected()) {
			try {
				return em.find(_ReturnType, object);
			} catch (Exception e) {
				logger.error("Failed to find EntityManager " + e);
			} finally {
				em.close();

			}
		}
		return null;
	}

	public <T> T remove(T object) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		if (em != null && connected()) {
			try {
				tx.begin();
				object = em.merge(object);
				em.remove(object);
				tx.commit();
			} catch (Exception e) {
				logger.error("Failed to remove EntityManager " + e);
			} finally {
				em.close();
				if (object != null && object instanceof AbstractEntity) {
					@SuppressWarnings("unchecked")
					T t = (T) findData(((AbstractEntity) object).getId(), object.getClass());
					return t;
				}
				
			}
		}
		return object;
	}
	
	public <T> SuccessState remove(List<T> list) {
		SuccessState state = new SuccessState();
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		if (em != null && connected()) {
			if (list != null) {
				for (T t : list) {
					try {
						tx.begin();
						t = em.merge(t);
						em.remove(t);
						tx.commit();
						state.addSuccess();
					} catch (Exception e) {
						logger.error("Failed to remove EntityManager " + e);
						state.addFailed();
					}
				}
			}
			em.close();
		}
		return state;
	}

	public <T> T merge(T object) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		if (em != null && connected()) {
			try {
				tx.begin();
				em.merge(object);
				tx.commit();
			} catch (Exception e) {
				tx.rollback();
				logger.error("Failed to merge EntityManager " + e);
			} finally {
				em.close();
			}
		}
		return object;
	}

	public <T> T persist(T object) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		if (em != null && connected()) {
			try {
				tx.begin();
				em.persist(object);
				tx.commit();
			} catch (Exception e) {
				logger.error("Failed to persist Object " + e);
			} finally {
				em.close();
			}
		}
		return object;
	}
	
	public <T> void persist(List<T> list) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		if (em != null && connected()) {
			try {
				if (list != null) {
					for (T t : list) {
						tx.begin();
						em.persist(t);
						tx.commit();
					}
				}
			} catch (Exception e) {
				logger.error("Failed to persist Object " + e);
			} finally {
				em.close();
			}
		}
	}
	
	public <T> T detach(T object) {
		EntityManager em = getEntityManager();
		EntityTransaction tx = em.getTransaction();
		if (em != null && connected()) {
			try {
				tx.begin();
				em.detach(object);
				tx.commit();
			} catch (Exception e) {
				logger.error("Failed to detach Object " + e);
			} finally {
				em.close();
			}
		}
		return object;
	}
	
	public static class DatabaseValueCache {
		public String value = "";
		public String key = "";
		
		public String getValue() {
			return this.value;
		}
		
		public void setValue(String value) {
			this.value = value;
		}
		
		public String getKey() {
			return this.key;
		}
		
		public void setKey(String key) {
			this.key = key;
		}
	}
}