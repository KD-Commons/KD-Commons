package de.kaesdingeling.commons.utils.enums;

public enum ELogLevel {
	NONE,
	INFO,
	WARNING,
	DEBUG,
	ERROR,
	CRITICAL_ERROR;
}