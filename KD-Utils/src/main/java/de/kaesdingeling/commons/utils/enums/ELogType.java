package de.kaesdingeling.commons.utils.enums;

public enum ELogType {
	UI,
	CRON,
	DATABASE,
	PLUGIN_MANAGER,
	PLUGIN,
	SYSTEM,
	UTILS,
	OTHER;
}