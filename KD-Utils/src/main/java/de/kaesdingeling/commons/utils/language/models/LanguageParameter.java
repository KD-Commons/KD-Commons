package de.kaesdingeling.commons.utils.language.models;

import de.kaesdingeling.commons.utils.language.interfaces.ILanguageParameter;
import lombok.Getter;

/**
 * 
 * @created 14.06.2020 - 15:45:20
 * @author KaesDingeling
 * @version 0.1
 */
@Getter
public class LanguageParameter implements ILanguageParameter {
	
	private ILanguageParameter subParameter;
	private String parameter;
	private String originalPrefix;
	
	/**
	 * 
	 * @param prefix
	 * @param parameter
	 * @return
	 * @Created 14.06.2020 - 15:45:18
	 * @author KaesDingeling
	 */
	public static ILanguageParameter get(String prefix, String parameter) {
		return get(null, prefix, parameter);
	}
	
	/**
	 * 
	 * @param subParameter
	 * @param parameter
	 * @return
	 * @Created 14.06.2020 - 15:45:16
	 * @author KaesDingeling
	 */
	public static ILanguageParameter get(ILanguageParameter subParameter, String parameter) {
		return new LanguageParameter(null, subParameter, parameter);
	}
	
	/**
	 * 
	 * @param subParameter
	 * @param prefix
	 * @param parameter
	 * @return
	 * @Created 14.06.2020 - 15:45:14
	 * @author KaesDingeling
	 */
	public static ILanguageParameter get(ILanguageParameter subParameter, String prefix, String parameter) {
		return new LanguageParameter(prefix, subParameter, parameter);
	}
	
	/**
	 * 
	 * @param prefix
	 * @param subParameter
	 * @param parameter
	 */
	public LanguageParameter(String prefix, ILanguageParameter subParameter, String parameter) {
		this.subParameter = subParameter;
		this.parameter = parameter;
		this.originalPrefix = prefix;
	}
}