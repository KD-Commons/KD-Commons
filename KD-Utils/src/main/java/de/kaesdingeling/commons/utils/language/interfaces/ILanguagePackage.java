package de.kaesdingeling.commons.utils.language.interfaces;

/**
 * 
 * @created 14.06.2020 - 13:36:58
 * @author KaesDingeling
 * @version 0.1
 */
public interface ILanguagePackage {
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 13:40:25
	 * @author KaesDingeling
	 */
	public default String getPrefix() {
		return getEntry().getPrefix();
	}
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 15:20:10
	 * @author KaesDingeling
	 */
	public ILanguageEntry getEntry();
	
	/**
	 * 
	 * @param key
	 * @param value
	 * @Created 14.06.2020 - 14:53:53
	 * @author KaesDingeling
	 */
	public default void addEntry(String parameter, String value) {
		getEntry().addEntry(parameter, value);
	}
	
	/**
	 * 
	 * @param parameter
	 * @param value
	 * @param suffix
	 * @Created 14.06.2020 - 15:55:37
	 * @author KaesDingeling
	 */
	public default void addEntry(String parameter, String suffix, String value) {
		getEntry().addEntry(parameter, suffix, value);
	}
	
	/**
	 * 
	 * @param <E>
	 * @param parameter
	 * @param value
	 * @Created 18.06.2020 - 21:29:03
	 * @author KaesDingeling
	 */
	public default <E extends Enum<E>> void addEntry(E parameter, String value) {
		getEntry().addEntry(parameter, value);
	}

	/**
	 * 
	 * @param <E>
	 * @param parameter
	 * @param suffix
	 * @param value
	 * @Created 18.06.2020 - 21:28:58
	 * @author KaesDingeling
	 */
	public default <E extends Enum<E>> void addEntry(E parameter, String suffix, String value) {
		getEntry().addEntry(parameter, suffix, value);
	}
	
	/**
	 * 
	 * @param parameter
	 * @param value
	 * @Created 18.06.2020 - 19:48:12
	 * @author KaesDingeling
	 */
	public default void addEntry(ILanguageParameter parameter, String value) {
		getEntry().addEntry(parameter, value);
	}
	
	/**
	 * 
	 * @param parameter
	 * @param suffix
	 * @param value
	 * @Created 18.06.2020 - 19:51:12
	 * @author KaesDingeling
	 */
	public default void addEntry(ILanguageParameter parameter, String suffix, String value) {
		getEntry().addEntry(parameter, suffix, value);
	}
	
	/**
	 * 
	 * @param parameter
	 * @param value
	 * @Created 18.06.2020 - 19:49:01
	 * @author KaesDingeling
	 */
	public default void addEntry(ILanguageParameter parameter, ILanguageParameter value) {
		getEntry().addEntry(parameter, value);
	}
	
	/**
	 * 
	 * @param parameter
	 * @param suffix
	 * @param value
	 * @Created 18.06.2020 - 19:51:37
	 * @author KaesDingeling
	 */
	public default void addEntry(ILanguageParameter parameter, String suffix, ILanguageParameter value) {
		getEntry().addEntry(parameter, suffix, value);
	}
}