package de.kaesdingeling.commons.utils.language.models;

import java.util.Map;

import com.google.common.collect.Maps;

import de.kaesdingeling.commons.utils.language.interfaces.ILanguageEntry;
import lombok.Data;

/**
 * 
 * @created 14.06.2020 - 14:52:46
 * @author KaesDingeling
 * @version 0.1
 */
@Data
public class LanguageEntry implements ILanguageEntry {
	
	private String prefix;
	private Map<String, String> entryMap = Maps.newHashMap();
	
	/**
	 * 
	 * @param prefix
	 * @return
	 * @Created 18.06.2020 - 19:53:18
	 * @author KaesDingeling
	 */
	public static LanguageEntry create(String prefix) {
		return new LanguageEntry(prefix);
	}
	
	/**
	 * 
	 * @param prefix
	 */
	public LanguageEntry(String prefix) {
		this.prefix = prefix;
	}
}