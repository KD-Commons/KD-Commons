package de.kaesdingeling.commons.utils.annotations;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface Cron {
	String minute() default "*";
	String hour() default "*";
	String dayAtMonth() default "*";
	String month() default "*";
	String dayAtWeek() default "*";
}