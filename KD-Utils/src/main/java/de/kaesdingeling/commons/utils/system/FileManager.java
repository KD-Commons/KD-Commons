package de.kaesdingeling.commons.utils.system;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.google.gson.Gson;

import de.kaesdingeling.commons.utils.Utils;
import de.kaesdingeling.commons.utils.data.Parameters;

public class FileManager {
	public static class Save {
		public static void saveJson(Object content, String file) throws Exception {
			saveJson(content, SystemParameters.get().get(Parameters.ROOT_DIRECTORY), file);
		}
		
		public static void saveJson(Object content, String path, String file) throws Exception {
			saveFile(Utils.toPrettyJson(content), path, file);
		}
		
		public static void saveFile(String content, String file) throws Exception {
			saveFile(content, SystemParameters.get().get(Parameters.ROOT_DIRECTORY), file);
		}
		
		public static void saveFile(String content, String path, String file) throws Exception {
			FileWriter fileWriter = new FileWriter(path + file);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
            bufferedWriter.write(content);
            bufferedWriter.close();
		}
	}
	
	public static class Find {
		public static List<File> findJar(String fileName) {
			return findJar(SystemParameters.get().get(Parameters.ROOT_DIRECTORY), fileName);
		}
		
		public static List<File> findJar(String path, String fileName) {
			List<File> foundedFiles = new ArrayList<File>();
			for (File file : find(path, fileName)) {
				if (file.getName().endsWith(".jar")) {
					foundedFiles.add(file);
				}
			}
			return foundedFiles;
		}
		
		public static List<File> find(String fileName) {
			return find(SystemParameters.get().get(Parameters.ROOT_DIRECTORY), fileName);
		}
		
		public static List<File> find(String path, String fileName) {
			List<File> foundedFiles = new ArrayList<File>();
			for (File file : new File(path + fileName).listFiles()) {
				if (!file.isDirectory() && !file.getName().trim().isEmpty()) {
					foundedFiles.add(file);
				}
			}
			return foundedFiles;
		}
	}
	
	public static class Load {
		public static <T> T loadJson(Class<T> _class, String file) throws Exception {
			return loadJson(_class, SystemParameters.get().get(Parameters.ROOT_DIRECTORY), file);
		}
		
		public static FileReader loadFile(String file) throws Exception {
			return loadFile(SystemParameters.get().get(Parameters.ROOT_DIRECTORY), file);
		}
		
		public static FileReader loadFile(String path, String file) throws Exception {
			return new FileReader(path + file);
		}
		
		public static String loadContent(String path, String file) throws Exception {
			FileReader fileReader = null;
			String line = null;
			String totalLines = "";
			fileReader = loadFile(path, file);
			BufferedReader bufferedReader = new BufferedReader(fileReader);
	        while ((line = bufferedReader.readLine()) != null) {
	        	totalLines += line;
	        }
	        bufferedReader.close();
			return totalLines;
		}
		
		public static <T> List<T> loadListJson(Class<T[]> _class, String path, String file) throws Exception {
			String totalLines = loadContent(path, file);
	        if (!totalLines.isEmpty()) {
	            return Arrays.asList(new Gson().fromJson(totalLines, _class));
	        }
			return null;
		}
		
		public static <T> T loadJson(Class<T> _class, String path, String file) throws Exception {
			String totalLines = loadContent(path, file);
	        if (!totalLines.isEmpty()) {
	            return new Gson().fromJson(totalLines, _class);
	        }
			return null;
		}
	}
}