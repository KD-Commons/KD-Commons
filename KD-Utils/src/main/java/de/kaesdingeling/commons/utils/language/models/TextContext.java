package de.kaesdingeling.commons.utils.language.models;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import com.google.common.collect.Lists;

import de.kaesdingeling.commons.utils.language.LanguageConfig;
import de.kaesdingeling.commons.utils.language.interfaces.ITextContext;
import de.kaesdingeling.commons.utils.language.interfaces.ITextParameter;
import lombok.Data;
import lombok.Getter;

/**
 * 
 * @created 14.06.2020 - 14:56:43
 * @author KaesDingeling
 * @version 0.1
 */
@Data
public class TextContext implements ITextContext<TextContext> {
	
	@Getter
	private String prefix;
	
	@Getter
	private List<ITextParameter<?>> parameter = Lists.newArrayList();
	
	/**
	 * 
	 * @param prefix
	 */
	public TextContext(String prefix) {
		this(prefix, null);
	}
	
	/**
	 * 
	 * @param prefix
	 * @param parameters
	 */
	public TextContext(String prefix, List<ITextParameter<?>> parameters) {
		this.prefix = prefix;
		
		if (parameters != null) {
			this.parameter.addAll(parameters);
		}
	}

	@Override
	public TextContext getParent() {
		if (!parameter.isEmpty()) {
			parameter.remove(parameter.size() - 1);
		}
		
		return this;
	}

	@Override
	public TextContext add(String parameter) {
		if (parameter.contains(" ")) {
			parameter = parameter.replaceAll(" ", LanguageConfig.SEPARATOR);
		}
		
		TextParameter textParameter = TextParameter.builder()
				.parameter(parameter)
				.build();
		
		this.parameter.add(textParameter);
		textParameter.setPosition(this.parameter.size());
		
		return this;
	}

	@Override
	public TextContext addNested(String parameter) {
		return this.clone().add(parameter);
	}

	@Override
	public String getParameters() {
		String line = null;
		
		parameter = parameter.stream().sorted(Comparator.comparingLong(ITextParameter::getPosition)).collect(Collectors.toList());
		
		for (ITextParameter<?> textParameter : parameter) {
			if (line == null) {
				line = textParameter.getParameter();
			} else {
				line += LanguageConfig.SEPARATOR + textParameter.getParameter();
			}
		}
		
		return line;
	}
	
	@Override
	public String toString() {
		String line = prefix.toLowerCase();
		parameter = parameter.stream().sorted(Comparator.comparingLong(ITextParameter::getPosition)).collect(Collectors.toList());
		
		for (ITextParameter<?> textParameter : parameter) {
			line += LanguageConfig.SEPARATOR + textParameter.getParameter();
		}
		
		return line;
	}

	@Override
	public TextContext clone() {
		List<ITextParameter<?>> parameters = Lists.newArrayList();
		
		for (ITextParameter<?> textParameter : this.parameter) {
			parameters.add(textParameter.clone());
		}
		
		return new TextContext(this.prefix, parameters);
	}
}