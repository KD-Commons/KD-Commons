package de.kaesdingeling.commons.utils.language;

import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.regex.Matcher;

import org.apache.commons.lang3.StringUtils;

import com.google.common.base.Stopwatch;
import com.google.common.collect.Maps;

import de.kaesdingeling.commons.utils.enums.ELogState;
import de.kaesdingeling.commons.utils.language.interfaces.ILanguageBundle;
import de.kaesdingeling.commons.utils.language.interfaces.ILanguagePackage;
import de.kaesdingeling.commons.utils.language.interfaces.ILanguageParameter;
import de.kaesdingeling.commons.utils.language.interfaces.ITextContext;
import de.kaesdingeling.commons.utils.language.models.TextContext;
import lombok.extern.log4j.Log4j2;

/**
 * 
 * @created 14.06.2020 - 15:59:58
 * @author KaesDingeling
 * @version 0.1
 */
@Log4j2
public class TextUtils {
	
	/**
	 * 
	 * @param <E>
	 * @param e
	 * @param objects
	 * @return
	 * @Created 18.06.2020 - 21:24:04
	 * @author KaesDingeling
	 */
	public static <E extends Enum<E>> String text(E e, Object... objects) {
		return text(e, LanguageConfig.DEFAULT_SUFFIX, objects);
	}
	
	/**
	 * 
	 * @param <E>
	 * @param e
	 * @param suffix
	 * @param objects
	 * @return
	 * @Created 18.06.2020 - 21:23:58
	 * @author KaesDingeling
	 */
	public static <E extends Enum<E>> String text(E e, String suffix, Object... objects) {
		return text(new TextContext("enum"), convertEnumToString(e), suffix, objects);
	}
	
	/**
	 * 
	 * @param <E>
	 * @param e
	 * @return
	 * @Created 18.06.2020 - 21:23:55
	 * @author KaesDingeling
	 */
	public static <E extends Enum<E>> String convertEnumToString(E e) {
		return StringUtils.lowerCase(e.getClass().getName()) + LanguageConfig.SEPARATOR + StringUtils.lowerCase(e.name());
	}
	
	/**
	 * 
	 * @param <E>
	 * @param e
	 * @return
	 * @Created 18.06.2020 - 21:23:52
	 * @author KaesDingeling
	 */
	public static <E extends Enum<E>> String convertEnumToStringWithPrefix(E e) {
		return StringUtils.lowerCase("enum") + LanguageConfig.SEPARATOR + StringUtils.lowerCase(e.getClass().getName()) + LanguageConfig.SEPARATOR + StringUtils.lowerCase(e.name());
	}
	
	/**
	 * 
	 * @param textContext
	 * @return
	 * @Created 14.06.2020 - 16:00:29
	 * @author KaesDingeling
	 */
	public static <T extends ITextContext<T>> String text(T textContext) {
		return text(textContext, null);
	}
	
	/**
	 * 
	 * @param <T>
	 * @param <P>
	 * @param parameter
	 * @param values
	 * @return
	 * @Created 14.06.2020 - 16:03:03
	 * @author KaesDingeling
	 */
	public static <P extends ILanguageParameter> String text(P parameter, Object... values) {
		return text(LanguageConfig.DEFAULT_SUFFIX, parameter, values);
	}
	
	/**
	 * 
	 * @param <P>
	 * @param locale
	 * @param parameter
	 * @param values
	 * @return
	 * @Created 18.06.2020 - 18:15:50
	 * @author KaesDingeling
	 */
	public static <P extends ILanguageParameter> String text(Locale locale, P parameter, Object... values) {
		return text(LanguageConfig.DEFAULT_SUFFIX, locale, parameter, values);
	}
	
	/**
	 * 
	 * @param <T>
	 * @param <P>
	 * @param suffix
	 * @param parameter
	 * @param values
	 * @return
	 * @Created 14.06.2020 - 16:03:36
	 * @author KaesDingeling
	 */
	public static <T extends ITextContext<T>, P extends ILanguageParameter> String text(String suffix, P parameter, Object... values) {
		return text(suffix, loadLocale(), parameter, values);
	}

	/**
	 * 
	 * @param <T>
	 * @param <P>
	 * @param suffix
	 * @param locale
	 * @param parameter
	 * @param values
	 * @return
	 * @Created 18.06.2020 - 18:03:18
	 * @author KaesDingeling
	 */
	@SuppressWarnings("unchecked")
	public static <T extends ITextContext<T>, P extends ILanguageParameter> String text(String suffix, Locale locale, P parameter, Object... values) {
		T textContext = (T) new TextContext(parameter.getPrefix());
		
		return text(suffix, locale, textContext, parameter.getFull(), values);
	}
	
	/**
	 * 
	 * @param <T>
	 * @param textContext
	 * @param name
	 * @param values
	 * @return
	 * @Created 14.06.2020 - 16:02:39
	 * @author KaesDingeling
	 */
	public static <T extends ITextContext<T>> String text(T textContext, String name, Object... values) {
		return text(LanguageConfig.DEFAULT_SUFFIX, textContext, name, values);
	}
	
	/**
	 * 
	 * @param <T>
	 * @param suffix
	 * @param textContext
	 * @return
	 * @Created 14.06.2020 - 16:02:36
	 * @author KaesDingeling
	 */
	public static <T extends ITextContext<T>> String text(String suffix, T textContext) {
		return text(suffix, loadLocale(), textContext, null);
	}
	
	/**
	 * 
	 * @param <T>
	 * @param suffix
	 * @param textContext
	 * @param name
	 * @param values
	 * @return
	 * @Created 14.06.2020 - 16:02:33
	 * @author KaesDingeling
	 */
	public static <T extends ITextContext<T>> String text(String suffix, T textContext, String name, Object... values) {
		return text(suffix, loadLocale(), textContext, name, values);
	}
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 16:37:51
	 * @author KaesDingeling
	 */
	public static Locale loadLocale() {
		return LanguageConfig.languageService
				.getDefault()
				.getPrimaryLocale();
	}
	
	/**
	 * 
	 * @param suffix
	 * @param locale
	 * @param textContext
	 * @param name
	 * @param values
	 * @return
	 * @Created 14.06.2020 - 16:37:54
	 * @author KaesDingeling
	 */
	public static <T extends ITextContext<T>> String text(String suffix, Locale locale, T textContext, String name, Object... values) {
		Stopwatch stopWatch = null;
		
		if (LanguageConfig.DEBUG) {
			stopWatch = Stopwatch.createStarted();
		}
		
		String parameter = buildParameter(suffix, textContext, name);
		String text = "";
		
		boolean foundEntry = false;
		try {
			for (ILanguageBundle bundle : LanguageConfig.languageService.getBundles()) {
				if (StringUtils.equals(bundle.getPrimaryLocale().getCountry(), locale.getCountry()) && StringUtils.equals(bundle.getPrimaryLocale().getLanguage(), locale.getLanguage())) {
					text = text(bundle, textContext.getPrefix(), parameter, values);
					foundEntry = true;
					break;
				}
			}
			
			if (!foundEntry) {
				for (ILanguageBundle bundle : LanguageConfig.languageService.getBundles()) {
					boolean match = false;
					
					for (Locale supportedLocale : bundle.getSupportedLocales()) {
						if (StringUtils.equals(supportedLocale.getCountry(), locale.getCountry()) && StringUtils.equals(supportedLocale.getLanguage(), locale.getLanguage())) {
							match = true;
						}
					}
					
					if (match) {
						text = text(bundle, textContext.getPrefix(), parameter, values);
						break;
					}
				}
			}
			
			if (StringUtils.isEmpty(text)) {
				text = text(LanguageConfig.languageService.getDefault(), textContext.getPrefix(), parameter, values);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		if (StringUtils.isEmpty(text)) {
			text = buildFullParameter(suffix, textContext, name);
			log.error(ELogState.FAILED + " to load textkey: " + text);
		}
		
		if (LanguageConfig.DEBUG && stopWatch != null) {
			stopWatch.stop();
			log.debug(ELogState.LOAD + "lang: " + buildFullParameter(suffix, textContext, name) + " <<>> " + text + " in " + stopWatch.elapsed().toMillis() + "ms");
		}
		
		return text;
	}
	
	/**
	 * 
	 * @param bundle
	 * @param prefix
	 * @param parameter
	 * @param values
	 * @return
	 * @Created 14.06.2020 - 16:30:38
	 * @author KaesDingeling
	 */
	public static String text(ILanguageBundle bundle, String prefix, String parameter, Object... values) {
		String text = "";
		
		Optional<ILanguagePackage> foundBundle = bundle.getPackageByPrefix(prefix);
		
		if (foundBundle.isPresent()) {
			text = foundBundle.get().getEntry().getEntryMap().get(parameter);
		}
		
		if (StringUtils.isNoneBlank(text) && text.contains("{")) {
			if (values != null) {
				for (int i = 0; i < values.length; i++) {
					if (text.contains("{" + i + "}")) {
						text = text.replace("{" + i + "}", values[i].toString());
					}
				}
			}
			
			Map<String, String> replaceMap = Maps.newLinkedHashMap();
			
			Matcher matcher = LanguageConfig.REFERENCE_PATTERN.matcher(text);
			
			while (matcher.find()) {
				String textLink = matcher.group().replace("@{", "").replace("}@", "");
				String linkPrefix = textLink.substring(0, textLink.indexOf("."));
				replaceMap.put(matcher.group(), text(bundle, linkPrefix, textLink.replaceAll(linkPrefix + LanguageConfig.SEPARATOR, ""), values));
			}
			
			for (String key : replaceMap.keySet()) {
				text = text.replace(key, replaceMap.get(key));
			}
		}
		
		return text;
	}
	
	/**
	 * 
	 * @param <T>
	 * @param textContext
	 * @param name
	 * @return
	 * @Created 14.06.2020 - 16:14:43
	 * @author KaesDingeling
	 */
	public static <T extends ITextContext<T>> String buildParameter(T textContext, String name) {
		return buildParameter(null, textContext, name);
	}
	
	/**
	 * 
	 * @param <T>
	 * @param suffix
	 * @param textContext
	 * @param name
	 * @return
	 * @Created 14.06.2020 - 16:14:40
	 * @author KaesDingeling
	 */
	public static <T extends ITextContext<T>> String buildParameter(String suffix, T textContext, String name) {
		String parameter = textContext.getParameters();
		
		if (parameter == null) {
			parameter = "";
		}
		
		if (StringUtils.isNotEmpty(name)) {
			parameter += LanguageConfig.SEPARATOR + fromatValue(name);
		}
		
		if (suffix != null) {
			parameter += LanguageConfig.SEPARATOR + suffix.toLowerCase();
		}
		
		if (parameter.startsWith(".")) {
			parameter = parameter.substring(1);
		}
		
		return parameter;
	}
	
	/**
	 * 
	 * @param <T>
	 * @param textContext
	 * @param name
	 * @return
	 * @Created 14.06.2020 - 16:14:38
	 * @author KaesDingeling
	 */
	public static <T extends ITextContext<T>> String buildFullParameter(T textContext, String name) {
		return buildFullParameter(null, textContext, name);
	}
	
	/**
	 * 
	 * @param <T>
	 * @param suffix
	 * @param textContext
	 * @param name
	 * @return
	 * @Created 14.06.2020 - 16:14:36
	 * @author KaesDingeling
	 */
	public static <T extends ITextContext<T>> String buildFullParameter(String suffix, T textContext, String name) {
		return textContext.getPrefix().toLowerCase() + LanguageConfig.SEPARATOR + buildParameter(suffix, textContext, name);	
	}
	
	/**
	 * 
	 * @param value
	 * @return
	 * @Created 14.06.2020 - 16:14:34
	 * @author KaesDingeling
	 */
	public static String fromatValue(String value) {
		if (value.contains(" ")) {
			value = value.replaceAll(" ", LanguageConfig.SEPARATOR);
		}
		
		return value;
	}
	
	/**
	 * 
	 * @param parameter
	 * @return
	 * @Created 14.06.2020 - 16:14:11
	 * @author KaesDingeling
	 */
	public static String textLink(ILanguageParameter parameter) {
		return textLink(parameter, LanguageConfig.DEFAULT_SUFFIX);
	}
	
	/**
	 * 
	 * @param parameter
	 * @param suffix
	 * @return
	 * @Created 14.06.2020 - 16:14:07
	 * @author KaesDingeling
	 */
	public static String textLink(ILanguageParameter parameter, String suffix) {
		return textLink(parameter.getPrefix(), parameter.getFull(), suffix);
	}
	
	/**
	 * 
	 * @param prefix
	 * @param name
	 * @return
	 * @Created 14.06.2020 - 16:14:04
	 * @author KaesDingeling
	 */
	public static String textLink(String prefix, String name) {
		return textLink(prefix, name, LanguageConfig.DEFAULT_SUFFIX);
	}
	
	/**
	 * 
	 * @param prefix
	 * @param name
	 * @param suffix
	 * @return
	 * @Created 14.06.2020 - 16:14:01
	 * @author KaesDingeling
	 */
	public static String textLink(String prefix, String name, String suffix) {
		String textLink = "@{" + prefix.toLowerCase() + LanguageConfig.SEPARATOR + name;
		
		if (suffix != null) {
			textLink += LanguageConfig.SEPARATOR + suffix.toLowerCase();
		}
		
		textLink += "}@";
		
		return textLink;
	}
}