package de.kaesdingeling.commons.utils.data;

import java.util.Date;

import de.kaesdingeling.commons.utils.enums.ELogLevel;
import de.kaesdingeling.commons.utils.enums.ELogType;

public class LogLine {
	private Class<?> _clazz;
	private ELogType logType;
	private ELogLevel logLevel;
	private Object message;
	private long created;
	
	public static LogLine get() {
		return new LogLine();
	}
	public Class<?> getClazz() {
		return _clazz;
	}
	public LogLine setClazz(Class<?> _clazz) {
		this._clazz = _clazz;
		return this;
	}
	public ELogType getLogType() {
		return logType;
	}
	public LogLine setLogType(ELogType logType) {
		this.logType = logType;
		return this;
	}
	public ELogLevel getLogLevel() {
		return logLevel;
	}
	public LogLine setLogLevel(ELogLevel logLevel) {
		this.logLevel = logLevel;
		return this;
	}
	public Object getMessage() {
		return message;
	}
	public LogLine setMessage(Object message) {
		this.message = message;
		return this;
	}
	public long getCreated() {
		return created;
	}
	public Date getCreatedDate() {
		return new Date(created);
	}
	public LogLine setCreated(long created) {
		this.created = created;
		return this;
	}
	public LogLine createdNow() {
		this.created = System.currentTimeMillis();
		return this;
	}
}