package de.kaesdingeling.commons.utils.database.abstracts;

import java.util.Date;

import javax.persistence.MappedSuperclass;

import lombok.Getter;
import lombok.Setter;

@MappedSuperclass
public abstract class AbstractWithCreatedEntity extends AbstractEntity {
	private static final long serialVersionUID = 4821097880073580588L;
	
	@Getter
	@Setter
	private long created;
	
	public void created() {
		created = System.currentTimeMillis();
	}
	
	public void setCreateDate(Date created) {
		this.created = created.getTime();
	}
}