package de.kaesdingeling.commons.utils.interfaces;

@FunctionalInterface
public interface TypedInputListener<T> {
	public T event();
}