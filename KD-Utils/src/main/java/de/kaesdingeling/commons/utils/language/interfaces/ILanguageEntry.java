package de.kaesdingeling.commons.utils.language.interfaces;

import java.util.Map;

import de.kaesdingeling.commons.utils.language.LanguageConfig;
import de.kaesdingeling.commons.utils.language.TextUtils;

/**
 * 
 * @created 14.06.2020 - 14:53:04
 * @author KaesDingeling
 * @version 0.1
 */
public interface ILanguageEntry {
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 14:53:13
	 * @author KaesDingeling
	 */
	public String getPrefix();
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 14:53:28
	 * @author KaesDingeling
	 */
	public Map<String, String> getEntryMap();
	
	/**
	 * 
	 * @param key
	 * @param value
	 * @Created 14.06.2020 - 14:53:53
	 * @author KaesDingeling
	 */
	public default void addEntry(String parameter, String value) {
		addEntry(parameter, LanguageConfig.DEFAULT_SUFFIX, value);
	}
	
	/**
	 * 
	 * @param parameter
	 * @param value
	 * @param suffix
	 * @Created 14.06.2020 - 15:55:37
	 * @author KaesDingeling
	 */
	public default void addEntry(String parameter, String suffix, String value) {
		getEntryMap().put(parameter + LanguageConfig.SEPARATOR + suffix, value);
	}
	
	/**
	 * 
	 * @param <E>
	 * @param parameter
	 * @param value
	 * @Created 18.06.2020 - 21:29:03
	 * @author KaesDingeling
	 */
	public default <E extends Enum<E>> void addEntry(E parameter, String value) {
		addEntry(parameter, LanguageConfig.DEFAULT_SUFFIX, value);
	}
	
	/**
	 * 
	 * @param <E>
	 * @param parameter
	 * @param value
	 * @Created 18.06.2020 - 21:37:07
	 * @author KaesDingeling
	 */
	public default <E extends Enum<E>> void addEntry(E parameter, ILanguageParameter value) {
		addEntry(parameter, LanguageConfig.DEFAULT_SUFFIX, value);
	}

	/**
	 * 
	 * @param <E>
	 * @param parameter
	 * @param suffix
	 * @param value
	 * @Created 18.06.2020 - 21:28:58
	 * @author KaesDingeling
	 */
	public default <E extends Enum<E>> void addEntry(E parameter, String suffix, String value) {
		addEntry(TextUtils.convertEnumToString(parameter), suffix, value);
	}
	
	/**
	 * 
	 * @param <E>
	 * @param parameter
	 * @param suffix
	 * @param value
	 * @Created 18.06.2020 - 21:36:57
	 * @author KaesDingeling
	 */
	public default <E extends Enum<E>> void addEntry(E parameter, String suffix, ILanguageParameter value) {
		addEntry(TextUtils.convertEnumToString(parameter), suffix, TextUtils.textLink(value));
	}
	
	/**
	 * 
	 * @param parameter
	 * @param value
	 * @Created 18.06.2020 - 19:48:12
	 * @author KaesDingeling
	 */
	public default void addEntry(ILanguageParameter parameter, String value) {
		addEntry(parameter.getFull(), value);
	}
	
	/**
	 * 
	 * @param parameter
	 * @param suffix
	 * @param value
	 * @Created 18.06.2020 - 19:51:12
	 * @author KaesDingeling
	 */
	public default void addEntry(ILanguageParameter parameter, String suffix, String value) {
		addEntry(parameter.getFull(), suffix, value);
	}
	
	/**
	 * 
	 * @param parameter
	 * @param value
	 * @Created 18.06.2020 - 19:49:01
	 * @author KaesDingeling
	 */
	public default void addEntry(ILanguageParameter parameter, ILanguageParameter value) {
		addEntry(parameter.getFull(), TextUtils.textLink(value));
	}
	
	/**
	 * 
	 * @param parameter
	 * @param suffix
	 * @param value
	 * @Created 18.06.2020 - 19:51:37
	 * @author KaesDingeling
	 */
	public default void addEntry(ILanguageParameter parameter, String suffix, ILanguageParameter value) {
		addEntry(parameter.getFull(), suffix, TextUtils.textLink(value));
	}
	
	/**
	 * 
	 * @param key
	 * @return
	 * @Created 14.06.2020 - 14:54:44
	 * @author KaesDingeling
	 */
	public default String getEntry(String key) {
		return getEntryMap().get(key);
	}
}