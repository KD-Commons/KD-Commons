package de.kaesdingeling.commons.utils.language.interfaces;

import java.util.List;
import java.util.Locale;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @created 14.06.2020 - 13:35:04
 * @author KaesDingeling
 * @version 0.1
 */
public interface ILanguageBundle {
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 13:36:38
	 * @author KaesDingeling
	 */
	public int getVersion();
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 13:50:25
	 * @author KaesDingeling
	 */
	public boolean isDefaultLocale();
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 13:42:30
	 * @author KaesDingeling
	 */
	public Locale getPrimaryLocale();
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 13:36:36
	 * @author KaesDingeling
	 */
	public List<Locale> getSupportedLocales();
	
	/**
	 * 
	 * @param <P>
	 * @return
	 * @Created 14.06.2020 - 13:37:41
	 * @author KaesDingeling
	 */
	public List<ILanguagePackage> getAllPackages();
	
	/**
	 * 
	 * @param <P>
	 * @param prefix
	 * @return
	 * @Created 14.06.2020 - 13:38:51
	 * @author KaesDingeling
	 */
	public default Optional<ILanguagePackage> getPackageByPrefix(String prefix) {
		return getAllPackages().stream().filter(p -> StringUtils.equals(p.getPrefix(), prefix)).findAny();
	}
}