package de.kaesdingeling.commons.utils.data;

public class SuccessState {
	private int success = 0;
	private int ignored = 0;
	private int failed = 0;
	
	public int getSuccess() {
		return success;
	}
	public SuccessState setSuccess(int success) {
		this.success = success;
		return this;
	}
	public SuccessState addSuccess() {
		this.success++;
		return this;
	}
	public int getIgnored() {
		return ignored;
	}
	public SuccessState setIgnored(int ignored) {
		this.ignored = ignored;
		return this;
	}
	public SuccessState addIgnored() {
		this.ignored++;
		return this;
	}
	public int getFailed() {
		return failed;
	}
	public SuccessState setFailed(int failed) {
		this.failed = failed;
		return this;
	}
	public SuccessState addFailed() {
		this.failed++;
		return this;
	}
}