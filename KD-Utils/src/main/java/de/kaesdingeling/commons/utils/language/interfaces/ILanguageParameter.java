package de.kaesdingeling.commons.utils.language.interfaces;

import org.apache.commons.lang3.StringUtils;

/**
 * 
 * @created 14.06.2020 - 15:40:26
 * @author KaesDingeling
 * @version 0.1
 */
public interface ILanguageParameter {
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 15:41:02
	 * @author KaesDingeling
	 */
	public ILanguageParameter getSubParameter();
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 15:41:00
	 * @author KaesDingeling
	 */
	public String getParameter();
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 15:40:57
	 * @author KaesDingeling
	 */
	public String getOriginalPrefix();
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 15:43:14
	 * @author KaesDingeling
	 */
	public default String get() {
		return getParameter();
	}
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 15:43:11
	 * @author KaesDingeling
	 */
	public default String getPrefix() {
		if (StringUtils.isBlank(getOriginalPrefix()) && getSubParameter() != null) {
			return getSubParameter().getPrefix();
		} else {
			return getOriginalPrefix();
		}
	}
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 15:42:55
	 * @author KaesDingeling
	 */
	public default String getFull() {
		String fullParamter = "";
		
		if (getSubParameter() != null) {
			fullParamter += getSubParameter().getFull();
		}
		
		if (StringUtils.isNoneBlank(fullParamter)) {
			fullParamter += ".";
		}
		
		fullParamter += getParameter();
		
		return fullParamter;
	}
}