package de.kaesdingeling.commons.utils.system;

import java.util.HashMap;
import java.util.Map;

import de.kaesdingeling.commons.utils.data.Parameters;
import de.kaesdingeling.commons.utils.interfaces.ISystemParameters;

public class SystemParameters implements ISystemParameters {
	protected static SystemParameters systemParameters = null;
	protected Map<Parameters<?>, Object> parameters = null;
	
	public static SystemParameters get() {
		if (systemParameters == null) {
			systemParameters = new SystemParameters();
		}
		return systemParameters;
	}
	
	private SystemParameters() {
		parameters = new HashMap<Parameters<?>, Object>();
		// block new instance
	}
	
	public SystemParameters init(String... args) {
		
		return this;
	}
	
	@Override
	public <T> SystemParameters add(Parameters<T> key, T value) {
		parameters.put(key, value);
		return this;
	}
	
	@Override
	public <T> SystemParameters set(Parameters<T> key, T value) {
		remove(key);
		parameters.put(key, value);
		return this;
	}
	
	@Override
	public <T> T get(Parameters<T> key) {
		return key.convert(parameters.get(key));
	}

	@Override
	public <T> SystemParameters remove(Parameters<T> key) {
		parameters.remove(key);
		return this;
	}
}