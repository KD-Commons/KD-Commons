package de.kaesdingeling.commons.utils.interfaces;

import de.kaesdingeling.commons.utils.data.Parameters;

public interface ISystemParameters {
	public <T> ISystemParameters add(Parameters<T> key, T value);
	public <T> ISystemParameters set(Parameters<T> key, T value);
	public <T> T get(Parameters<T> key);
	public <T> ISystemParameters remove(Parameters<T> key);
}