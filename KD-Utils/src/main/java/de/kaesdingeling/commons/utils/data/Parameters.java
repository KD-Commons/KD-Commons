package de.kaesdingeling.commons.utils.data;

import java.util.Set;

import de.kaesdingeling.commons.utils.enums.ELogLevel;
import de.kaesdingeling.commons.utils.enums.ELogType;
import de.kaesdingeling.commons.utils.system.DatabaseConfig;

/**
 * 
 * @created 06.09.2020 - 15:49:20
 * @author KaesDingeling
 * @version 0.1
 */
public class Parameters<T> {
	
	/* System */
	public static final Parameters<Boolean> DEBUG = new Parameters<Boolean>();
	public static final Parameters<DatabaseConfig> DATABASE_CONFIG = new Parameters<DatabaseConfig>();
	
	/* Logger */
	public static final Parameters<Set<ELogLevel>> LOGGER_SHOW_LOG_LEVELS = new Parameters<Set<ELogLevel>>();
	public static final Parameters<Set<ELogType>> LOGGER_SHOW_LOG_TYPES = new Parameters<Set<ELogType>>();
	
	/* Directorys */
	public static final Parameters<String> ROOT_DIRECTORY = new Parameters<String>();
	public static final Parameters<String> LANG_DIRECTORY = new Parameters<String>();
	public static final Parameters<String> CONFIGS_DIRECTORY = new Parameters<String>();
	public static final Parameters<String> PLUGINS_DIRECTORY = new Parameters<String>();
	public static final Parameters<String> PLUGINS_TEMP_DIRECTORY = new Parameters<String>();
	
	/**
	 * 
	 * @param object
	 * @return
	 * @Created 06.09.2020 - 15:49:24
	 * @author KaesDingeling
	 */
	@SuppressWarnings("unchecked")
	public T convert(final Object object) {
		if (object == null) {
			return null;
		}
		
		return (T) object;
	}
}