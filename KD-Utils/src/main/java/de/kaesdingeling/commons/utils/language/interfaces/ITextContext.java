package de.kaesdingeling.commons.utils.language.interfaces;

import java.util.List;

/**
 * 
 * @created 14.06.2020 - 14:05:03
 * @author KaesDingeling
 * @version 0.1
 */
public interface ITextContext<T extends ITextContext<T>> extends Cloneable {
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 14:05:13
	 * @author KaesDingeling
	 */
	public String getPrefix();
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 14:05:33
	 * @author KaesDingeling
	 */
	public ITextContext<T> getParent();
	
	/**
	 * 
	 * @param parameter
	 * @return
	 * @Created 14.06.2020 - 14:06:15
	 * @author KaesDingeling
	 */
	public ITextContext<T> add(String parameter);
	
	/**
	 * 
	 * @param parameter
	 * @return
	 * @Created 14.06.2020 - 14:05:52
	 * @author KaesDingeling
	 */
	public ITextContext<T> addNested(String parameter);
	
	/**
	 * 
	 * @param <P>
	 * @return
	 * @Created 14.06.2020 - 14:46:56
	 * @author KaesDingeling
	 */
	public <P extends ITextParameter<P>> List<P> getParameter();
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 14:47:35
	 * @author KaesDingeling
	 */
	public String getParameters();
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 14:33:50
	 * @author KaesDingeling
	 */
	public ITextContext<T> clone();
}