package de.kaesdingeling.commons.utils.interfaces;

@FunctionalInterface
public interface TypedOutputListener<T> {
	public void event(T t);
}