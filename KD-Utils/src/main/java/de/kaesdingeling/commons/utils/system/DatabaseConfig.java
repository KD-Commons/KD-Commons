package de.kaesdingeling.commons.utils.system;

import java.util.HashMap;
import java.util.Map;

import de.kaesdingeling.commons.utils.data.Parameters;
import de.kaesdingeling.commons.utils.enums.ELogType;

public class DatabaseConfig {
	protected static Logger logger = Logger.get(DatabaseConfig.class, ELogType.DATABASE);
	
	private String driver = "com.mysql.jdbc.Driver";
	private String driverPrefix = "mysql";
	private String url = "localhost";
	private int port = 3306;
	private String database = "kaesframe";
	private String user = "root";
	private String password = "";
	private boolean useSSL = false;
	private boolean autoReconnect = true;
	private String useUnicode = "yes";
	private String characterEncoding = "UTF-8";
	private Map<String, String> customHibernateParameters = new HashMap<String, String>();
	
	public Map<String, String> buildConnection() {
		Map<String, String> properties = new HashMap<String, String>();
		properties.put("javax.persistence.jdbc.driver", driver);
		properties.put("javax.persistence.jdbc.url", "jdbc:" + driverPrefix + "://" + url + ":" + port + "/" + database + "?useSSL=" + String.valueOf(useSSL) + "&useUnicode=" + useUnicode + "&characterEncoding=" + characterEncoding + "&autoReconnect=" + String.valueOf(autoReconnect));
		properties.put("javax.persistence.jdbc.user", user);
		properties.put("javax.persistence.jdbc.password", password);
		if (customHibernateParameters != null && customHibernateParameters.size() > 0) {
			for (String key : customHibernateParameters.keySet()) {
				properties.put(key, customHibernateParameters.get(key));
			}
		}
		return properties;
	}
	
	public static DatabaseConfig get() {
		DatabaseConfig databaseConfig = SystemParameters.get().get(Parameters.DATABASE_CONFIG);
		if (databaseConfig == null) {
			try {
				loadDatabase();
			} catch (Exception e) {
				logger.critticalError("Database config can not load: " + e);
				SystemParameters.get().set(Parameters.DATABASE_CONFIG, new DatabaseConfig());
				try {
					saveDatabase();
				} catch (Exception e2) {
				}
			} finally {
				databaseConfig = SystemParameters.get().get(Parameters.DATABASE_CONFIG);
			}
		}
		return databaseConfig;
	}
	
	public static void saveDatabase() throws Exception {
		FileManager.Save.saveJson(SystemParameters.get().get(Parameters.DATABASE_CONFIG), SystemParameters.get().get(Parameters.CONFIGS_DIRECTORY), "Database.json");
	}
	
	public static void loadDatabase() throws Exception {
		SystemParameters.get().set(Parameters.DATABASE_CONFIG, FileManager.Load.loadJson(DatabaseConfig.class, SystemParameters.get().get(Parameters.CONFIGS_DIRECTORY), "Database.json"));
	}

	public String getDriver() {
		return driver;
	}
	public DatabaseConfig setDriver(String driver) {
		this.driver = driver;
		return this;
	}
	public String getDriverPrefix() {
		return driverPrefix;
	}
	public DatabaseConfig setDriverPrefix(String driverPrefix) {
		this.driverPrefix = driverPrefix;
		return this;
	}
	public String getUrl() {
		return url;
	}
	public DatabaseConfig setUrl(String url) {
		this.url = url;
		return this;
	}
	public int getPort() {
		return port;
	}
	public DatabaseConfig setPort(int port) {
		this.port = port;
		return this;
	}
	public String getDatabase() {
		return database;
	}
	public DatabaseConfig setDatabase(String database) {
		this.database = database;
		return this;
	}
	public String getUser() {
		return user;
	}
	public DatabaseConfig setUser(String user) {
		this.user = user;
		return this;
	}
	public String getPassword() {
		return password;
	}
	public DatabaseConfig setPassword(String password) {
		this.password = password;
		return this;
	}
	public boolean isUseSSL() {
		return useSSL;
	}
	public DatabaseConfig setUseSSL(boolean useSSL) {
		this.useSSL = useSSL;
		return this;
	}
	public boolean isAutoReconnect() {
		return autoReconnect;
	}
	public DatabaseConfig setAutoReconnect(boolean autoReconnect) {
		this.autoReconnect = autoReconnect;
		return this;
	}
	public String getUseUnicode() {
		return useUnicode;
	}
	public DatabaseConfig setUseUnicode(String useUnicode) {
		this.useUnicode = useUnicode;
		return this;
	}
	public String getCharacterEncoding() {
		return characterEncoding;
	}
	public DatabaseConfig setCharacterEncoding(String characterEncoding) {
		this.characterEncoding = characterEncoding;
		return this;
	}
	public Map<String, String> getCustomHibernateParameters() {
		return customHibernateParameters;
	}
	public DatabaseConfig setCustomHibernateParameters(Map<String, String> customHibernateParameters) {
		this.customHibernateParameters = customHibernateParameters;
		return this;
	}
}