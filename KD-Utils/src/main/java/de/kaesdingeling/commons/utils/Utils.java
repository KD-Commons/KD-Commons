package de.kaesdingeling.commons.utils;

import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.net.URISyntaxException;
import java.nio.file.Files;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.SerializationUtils;

import com.google.gson.GsonBuilder;

public class Utils {
	public static File getFile(String fileName) {
		return new File(Utils.class.getClassLoader().getResource(fileName).getFile());
	}
	
	public static String toPrettyJson(final Object object) {
		return new GsonBuilder().setPrettyPrinting().create().toJson(object);
	}
	
	public static <T extends Serializable> T clone(T t) {
		return SerializationUtils.clone(t);
	}
	
	public static byte[] getBytes(File file) throws IOException {
		return Files.readAllBytes(file.toPath());
	}
	
	public static byte[] getBytes(Class<?> _clazz, String filePath, String fileName) throws IOException {
		return IOUtils.toByteArray(_clazz.getClassLoader().getResource(filePath + "/" + fileName));
	}
	
	public static File[] getFiles(Class<?> _clazz, String filePath) throws IOException, URISyntaxException {
		return new File(_clazz.getClassLoader().getResource(filePath).getFile()).listFiles();
	}
	
	public static String formatMoney(Double value, Locale locale, boolean withSymbol) {
		if (value != null) {
			if (locale != null) {
				value = value * 100;
				value = value.intValue() / 100.00;
				
				String returnString = NumberFormat.getInstance(locale).format(value);
				if (withSymbol) {
					returnString += " " + Currency.getInstance(locale).getSymbol();
				}
				return fixMoney(returnString);
			} else {
				return String.valueOf(value);
			}
		} else {
			return "";
		}
	}
	
	public static String fixMoney(String value) {
		if (value == null) {
			value = "";
		}
		
		if (!value.contains(",") && !value.contains(" ")) {
			value += ",00";
		} else if (value.contains(",")) {
			int counter = 0;
			String cache1 = null;
			String cache2 = null;
			
			for (String cache : value.split(",")) {
				if (counter == 0) {
					cache1 = cache;
					counter = 1;
				} else if (counter == 1) {
					cache2 = cache;
					counter = 2;
				}
			}
			
			if (cache1 != null && cache2 != null) {
				String cache3 = null;
				String cache4 = null;
				
				if (cache2.contains(" ")) {
					counter = 0;
					
					for (String cache : cache2.split(" ")) {
						if (counter == 0) {
							cache3 = cache;
							counter = 1;
						} else if (counter == 1) {
							cache4 = cache;
							counter = 2;
						}
					}
				} else {
					cache3 = cache2;
				}
				
				if (cache3.length() == 0) {
					cache3 += "00";
				} else if (cache3.length() == 1) {
					cache3 += "0";
				}
				
				value = cache1 + "," + cache3;
				
				if (cache4 != null) {
					value += " " + cache4;
				}
			}
		}
		
		return value;
	}
}