package de.kaesdingeling.commons.components.test.data.model;

import java.util.List;

import lombok.Data;

@Data
public class FAItemData {
	
	private String label;
	private String name;
	private String icon;
	private String unicode;
	private String objectID;
	
	private List<String> styles;
}