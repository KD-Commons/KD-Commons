package de.kaesdingeling.commons.components.test.data.model;

import java.util.List;

import lombok.Data;

@Data
public class FAMainData {
	
	private List<FAItemData> hits;
}