package de.kaesdingeling.commons.components.test;

import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.InputStreamReader;

import org.junit.jupiter.api.Test;

import com.google.gson.Gson;

import de.kaesdingeling.commons.components.test.data.model.FAMainData;

/**
 * 
 * @created 16.07.2020 - 10:52:02
 * @author KaesDingeling
 * @version 0.1
 */
public class FontAwesomeCheckTest {
	
	public static final String ICONS = "";
	
	/**
	 * 
	 * 
	 * @Created 16.07.2020 - 10:52:05
	 * @author KaesDingeling
	 */
	@Test
	public void check() {
		FAMainData data = new Gson().fromJson(new InputStreamReader(getClass().getClassLoader().getResourceAsStream("icons.json")), FAMainData.class);
		
		assertNotNull(data);
		
		System.out.println(data.getHits().size());
	}
}