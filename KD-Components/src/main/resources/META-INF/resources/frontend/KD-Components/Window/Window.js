import { PolymerElement } from '@polymer/polymer/polymer-element.js';
import { GestureEventListeners } from '@polymer/polymer/lib/mixins/gesture-event-listeners.js';
import { ThemableMixin } from '@vaadin/vaadin-themable-mixin/vaadin-themable-mixin.js';
import { ControlStateMixin } from '@vaadin/vaadin-control-state-mixin/vaadin-control-state-mixin.js';
import { ElementMixin } from '@vaadin/vaadin-element-mixin/vaadin-element-mixin.js';
import { html } from '@polymer/polymer/lib/utils/html-tag.js';

import '@vaadin/vaadin-button/src/vaadin-button.js';

function cleanUp(element) {
	element.$.window.classList.remove("noAnimation");
	document.onmouseup = null;
	document.onmousemove = null;
}

function init(element) {
	element.$.window.classList.add("noAnimation");
	document.onmouseup = function() { cleanUp(element) };
	return element.$;
}

function focus(element) {
	if (!element.classList.contains("focus")) {
		var body = document.body.childNodes;
		var i;
		for (i = 0; i < body.length; i++) {
			if (body[i].nodeName != null && body[i].nodeName.toLowerCase() == "window-overlay") {
				if (body[i].classList.contains("focus")) {
					body[i].classList.remove("focus");
				}
			}
		}
		
		element.classList.add("focus");
	}
}

function cut(value, maxValue) {
	if (value < 0) {
		return 0;
	} else if (value > maxValue) {
		return maxValue
	} else {
		return value;
	}
}

class WindowElement extends ElementMixin(ControlStateMixin(ThemableMixin(GestureEventListeners(PolymerElement)))) {
	
	static get is() {
		return 'window-overlay';
	}
	
	static get version() {
		return '1.0.0';
	}
	
	ready() {
		super.ready();
		focus(this);
        
        var system = this;
        var window = system.$;
        
        window.dragArea.addEventListener("dblclick", function(event) {
        	system.toggleExpand();
        });
	}
	
	get focusElement() {
		return this.$.window;
	}
	
	windowClick(event) {
		focus(this);
	}
	
	center() {
		var window = this.$;
        
		window.window.style.top = ((this.clientHeight / 2) - (window.window.clientHeight / 2)) + "px";
        window.window.style.left = ((this.clientWidth / 2) - (window.window.clientWidth / 2)) + "px";
	}
	
	move(event) {
		event.preventDefault();
		focus(this);
		
		var system = this;
		var window = init(system);
		
		var currClientX = event.clientX;
		var currClientY = event.clientY;
		var diffClientX = null;
		var diffClientY = null;
		var ignoreX = 0;
		var ignoreY = 0;
		
		document.onmousemove = function(event) {
			event.preventDefault();
			
			var currX = cut(event.clientX, system.clientWidth);
			var currY = cut(event.clientY, system.clientHeight);
			
			diffClientX = currClientX - currX;
			diffClientY = currClientY - currY;
			currClientX = currX;
			currClientY = currY;
			
			var top = (parseInt(window.window.offsetTop) - diffClientY);
			var left = (parseInt(window.window.offsetLeft) - diffClientX);
			var totalHeight = (system.clientHeight - 10);
			
			if (top > 0 && ignoreY < 0) {
				top = ignoreY + top;
				ignoreY = 0;
			}
			
			if (top < 0) {
				ignoreY = ignoreY + top;
				top = 0;
			}
            
			window.window.style.top = top + "px";
			window.window.style.left = left + "px";
		};
	}
	
	resize(event) {
        event.preventDefault();
        focus(this);
        
        var system = this;
        var window = init(system);
        
        var currClientX = event.clientX;
        var currClientY = event.clientY;
        var diffClientX = null;
        var diffClientY = null;
        var ignoreX = 0;
        var ignoreY = 0;
        
        document.onmousemove = function(event) {
        	event.preventDefault();
        	
        	var currX = cut(event.clientX, system.clientWidth);
        	var currY = cut(event.clientY, system.clientHeight);
        	
        	diffClientX = currClientX - currX;
        	diffClientY = currClientY - currY;
        	currClientX = currX;
        	currClientY = currY;
            
            var width = (parseInt(window.window.style.width) - diffClientX) - 36;
            var height = (parseInt(window.window.style.height) - diffClientY) - 36;
            
            if (width > 0 && ignoreX < 0) {
            	width = ignoreX + width;
            	ignoreX = 0;
            }
            
            if (width < 0) {
            	ignoreX = ignoreX + width;
            	width = 0;
            }
            
            if (height > 0 && ignoreY < 0) {
            	height = ignoreY + height;
            	ignoreY = 0;
            }
            
            if (height < 0) {
            	ignoreY = ignoreY + height;
            	height = 0;
			}
            
			window.window.style.width = (width + 36) + "px";
			window.window.style.height = (height + 36) + "px";
		};
	}
	
	static get template() {
		return html`
			<style include="shared-styles">
				@keyframes showWindow {
					0% {
						opacity: 0;
					}
				}
				
				@keyframes hideWindow {
					0% {
						-webkit-transform: scale(1);
						transform: scale(1);
						opacity: 1;
					} 100% {
						-webkit-transform: scale(0.8);
						transform: scale(0.8);
						opacity: 0;
					}
				}
			
				:host {
					-webkit-transition: visibility 140ms linear;
					transition: visibility 140ms linear;
					-webkit-animation: showWindow 140ms;
					animation: showWindow 140ms;
					-webkit-transform: scale(1);
					transform: scale(1);
					pointer-events: none;
					visibility: visible;
					position: fixed;
					height: 100%;
					width: 100%;
					opacity: 1;
					padding: 0;
					margin: 0;
					left: 0;
					top: 0;
				}
				
				:host(.hide) {
					-webkit-animation: hideWindow 140ms;
					animation: hideWindow 140ms;
					visibility: hidden;
					opacity: 0;
				}
				
				:host(.focus) {
					z-index: 200;
				}
				
				:host #window {
					box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1), 0 16px 80px -6px rgba(0, 0, 0, 0.15), 0 0 0 1px rgba(0, 0, 0, 0.09098);
					background-color: var(--lumo-base-color);
					border-radius: 4px;
					line-height: var(--lumo-line-height-m);
					font-family: var(--lumo-font-family);
					font-size: var(--lumo-font-size-m);
					color: var(--lumo-body-text-color);
					flex-direction: column;
					pointer-events: all;
					position: absolute;
					display: flex;
					font-weight: 400;
					letter-spacing: 0;
					text-transform: none;
					overflow: hidden;
					-webkit-text-size-adjust: 100%;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
				}
				
				:host #header {
					border-bottom: 1px solid rgb(204, 204, 204);
					background: rgb(227, 228, 230);
					position: relative;
					display: flex;
					height: 32px;
					width: 100%;
				}
				
				:host #header #dragArea {
					white-space: nowrap;
					overflow: hidden;
					display: flex;
					cursor: move;
					height: 100%;
					flex: 1;
				}
				
				:host #header #dragArea label {
					text-overflow: ellipsis;
					vertical-align: middle;
					display: inline-block;
					padding: 0 5px 0 10px;
					white-space: nowrap;
					line-height: 32px;
					overflow: hidden;
					height: 100%;
				}
				
				:host #header #windowAction {
					display: flex;
					height: 100%;
				}
				
				:host #window.expand {
					border-radius: 0 !important;
					height: 100% !important;
					width: 100% !important;
					left: 0 !important;
					top: 0 !important;
				}
				
				:host #window:not(.noAnimation) {
					-webkit-transition: border-radius 0.2s ease-in-out,
								height 0.2s ease-in-out,
								width 0.2s ease-in-out,
								left 0.2s ease-in-out,
								top 0.2s ease-in-out;
					transition: border-radius 0.2s ease-in-out,
								height 0.2s ease-in-out,
								width 0.2s ease-in-out,
								left 0.2s ease-in-out,
								top 0.2s ease-in-out;
				}
				
				:host #windowAction button.minimize > span > span {
				    box-shadow: transparent 0 0,transparent 1px 0,transparent 2px 0,transparent 3px 0,transparent 4px 0,transparent 5px 0,transparent 6px 0,transparent 7px 0,transparent 8px 0,transparent 9px 0,transparent 10px 0,transparent 11px 0,transparent 0 1px,transparent 1px 1px,transparent 2px 1px,transparent 3px 1px,transparent 4px 1px,transparent 5px 1px,transparent 6px 1px,transparent 7px 1px,transparent 8px 1px,transparent 9px 1px,transparent 10px 1px,transparent 11px 1px,transparent 0 2px,transparent 1px 2px,transparent 2px 2px,transparent 3px 2px,transparent 4px 2px,transparent 5px 2px,transparent 6px 2px,transparent 7px 2px,transparent 8px 2px,transparent 9px 2px,transparent 10px 2px,transparent 11px 2px,transparent 0 3px,transparent 1px 3px,transparent 2px 3px,transparent 3px 3px,transparent 4px 3px,transparent 5px 3px,transparent 6px 3px,transparent 7px 3px,transparent 8px 3px,transparent 9px 3px,transparent 10px 3px,transparent 11px 3px,transparent 0 4px,transparent 1px 4px,transparent 2px 4px,transparent 3px 4px,transparent 4px 4px,transparent 5px 4px,transparent 6px 4px,transparent 7px 4px,transparent 8px 4px,transparent 9px 4px,transparent 10px 4px,transparent 11px 4px,transparent 0 5px, rgb(100, 100, 100) 1px 5px, rgb(100, 100, 100) 2px 5px, rgb(100, 100, 100) 3px 5px, rgb(100, 100, 100) 4px 5px, rgb(100, 100, 100) 5px 5px, rgb(100, 100, 100) 6px 5px, rgb(100, 100, 100) 7px 5px, rgb(100, 100, 100) 8px 5px, rgb(100, 100, 100) 9px 5px, rgb(100, 100, 100) 10px 5px,transparent 11px 5px,transparent 0 6px,transparent 1px 6px,transparent 2px 6px,transparent 3px 6px,transparent 4px 6px,transparent 5px 6px,transparent 6px 6px,transparent 7px 6px,transparent 8px 6px,transparent 9px 6px,transparent 10px 6px,transparent 11px 6px,transparent 0 7px,transparent 1px 7px,transparent 2px 7px,transparent 3px 7px,transparent 4px 7px,transparent 5px 7px,transparent 6px 7px,transparent 7px 7px,transparent 8px 7px,transparent 9px 7px,transparent 10px 7px,transparent 11px 7px,transparent 0 8px,transparent 1px 8px,transparent 2px 8px,transparent 3px 8px,transparent 4px 8px,transparent 5px 8px,transparent 6px 8px,transparent 7px 8px,transparent 8px 8px,transparent 9px 8px,transparent 10px 8px,transparent 11px 8px,transparent 0 9px,transparent 1px 9px,transparent 2px 9px,transparent 3px 9px,transparent 4px 9px,transparent 5px 9px,transparent 6px 9px,transparent 7px 9px,transparent 8px 9px,transparent 9px 9px,transparent 10px 9px,transparent 11px 9px,transparent 0 10px,transparent 1px 10px,transparent 2px 10px,transparent 3px 10px,transparent 4px 10px,transparent 5px 10px,transparent 6px 10px,transparent 7px 10px,transparent 8px 10px,transparent 9px 10px,transparent 10px 10px,transparent 11px 10px,transparent 0 11px,transparent 1px 11px,transparent 2px 11px,transparent 3px 11px,transparent 4px 11px,transparent 5px 11px,transparent 6px 11px,transparent 7px 11px,transparent 8px 11px,transparent 9px 11px,transparent 10px 11px,transparent 11px 11px;
				}
				
				:host #windowAction button.compress > span > span {
					box-shadow: transparent 0 0,transparent 1px 0,transparent 2px 0,transparent 3px 0,transparent 4px 0,transparent 5px 0,transparent 6px 0,transparent 7px 0,transparent 8px 0,transparent 9px 0,transparent 10px 0,transparent 11px 0,transparent 0 1px,transparent 1px 1px,transparent 2px 1px, rgb(100, 100, 100) 3px 1px, rgb(100, 100, 100) 4px 1px, rgb(100, 100, 100) 5px 1px, rgb(100, 100, 100) 6px 1px, rgb(100, 100, 100) 7px 1px, rgb(100, 100, 100) 8px 1px, rgb(100, 100, 100) 9px 1px, rgb(100, 100, 100) 10px 1px,transparent 11px 1px,transparent 0 2px,transparent 1px 2px,transparent 2px 2px, rgb(100, 100, 100) 3px 2px,transparent 4px 2px,transparent 5px 2px,transparent 6px 2px,transparent 7px 2px,transparent 8px 2px,transparent 9px 2px, rgb(100, 100, 100) 10px 2px,transparent 11px 2px,transparent 0 3px, rgb(100, 100, 100) 1px 3px, rgb(100, 100, 100) 2px 3px, rgb(100, 100, 100) 3px 3px, rgb(100, 100, 100) 4px 3px, rgb(100, 100, 100) 5px 3px, rgb(100, 100, 100) 6px 3px, rgb(100, 100, 100) 7px 3px, rgb(100, 100, 100) 8px 3px,transparent 9px 3px, rgb(100, 100, 100) 10px 3px,transparent 11px 3px,transparent 0 4px, rgb(100, 100, 100) 1px 4px,transparent 2px 4px,transparent 3px 4px,transparent 4px 4px,transparent 5px 4px,transparent 6px 4px,transparent 7px 4px, rgb(100, 100, 100) 8px 4px,transparent 9px 4px, rgb(100, 100, 100) 10px 4px,transparent 11px 4px,transparent 0 5px, rgb(100, 100, 100) 1px 5px,transparent 2px 5px,transparent 3px 5px,transparent 4px 5px,transparent 5px 5px,transparent 6px 5px,transparent 7px 5px, rgb(100, 100, 100) 8px 5px,transparent 9px 5px, rgb(100, 100, 100) 10px 5px,transparent 11px 5px,transparent 0 6px, rgb(100, 100, 100) 1px 6px,transparent 2px 6px,transparent 3px 6px,transparent 4px 6px,transparent 5px 6px,transparent 6px 6px,transparent 7px 6px, rgb(100, 100, 100) 8px 6px,transparent 9px 6px, rgb(100, 100, 100) 10px 6px,transparent 11px 6px,transparent 0 7px, rgb(100, 100, 100) 1px 7px,transparent 2px 7px,transparent 3px 7px,transparent 4px 7px,transparent 5px 7px,transparent 6px 7px,transparent 7px 7px, rgb(100, 100, 100) 8px 7px,transparent 9px 7px, rgb(100, 100, 100) 10px 7px,transparent 11px 7px,transparent 0 8px, rgb(100, 100, 100) 1px 8px,transparent 2px 8px,transparent 3px 8px,transparent 4px 8px,transparent 5px 8px,transparent 6px 8px,transparent 7px 8px, rgb(100, 100, 100) 8px 8px, rgb(100, 100, 100) 9px 8px, rgb(100, 100, 100) 10px 8px,transparent 11px 8px,transparent 0 9px, rgb(100, 100, 100) 1px 9px,transparent 2px 9px,transparent 3px 9px,transparent 4px 9px,transparent 5px 9px,transparent 6px 9px,transparent 7px 9px, rgb(100, 100, 100) 8px 9px,transparent 9px 9px,transparent 10px 9px,transparent 11px 9px,transparent 0 10px, rgb(100, 100, 100) 1px 10px, rgb(100, 100, 100) 2px 10px, rgb(100, 100, 100) 3px 10px, rgb(100, 100, 100) 4px 10px, rgb(100, 100, 100) 5px 10px, rgb(100, 100, 100) 6px 10px, rgb(100, 100, 100) 7px 10px, rgb(100, 100, 100) 8px 10px,transparent 9px 10px,transparent 10px 10px,transparent 11px 10px,transparent 0 11px,transparent 1px 11px,transparent 2px 11px,transparent 3px 11px,transparent 4px 11px,transparent 5px 11px,transparent 6px 11px,transparent 7px 11px,transparent 8px 11px,transparent 9px 11px,transparent 10px 11px,transparent 11px 11px;
				}
				
				:host #windowAction button.expand > span > span {
					box-shadow: transparent 0 0,transparent 1px 0,transparent 2px 0,transparent 3px 0,transparent 4px 0,transparent 5px 0,transparent 6px 0,transparent 7px 0,transparent 8px 0,transparent 9px 0,transparent 10px 0,transparent 11px 0,transparent 0 1px, rgb(100, 100, 100) 1px 1px, rgb(100, 100, 100) 2px 1px, rgb(100, 100, 100) 3px 1px, rgb(100, 100, 100) 4px 1px, rgb(100, 100, 100) 5px 1px, rgb(100, 100, 100) 6px 1px, rgb(100, 100, 100) 7px 1px, rgb(100, 100, 100) 8px 1px, rgb(100, 100, 100) 9px 1px, rgb(100, 100, 100) 10px 1px,transparent 11px 1px,transparent 0 2px, rgb(100, 100, 100) 1px 2px,transparent 2px 2px,transparent 3px 2px,transparent 4px 2px,transparent 5px 2px,transparent 6px 2px,transparent 7px 2px,transparent 8px 2px,transparent 9px 2px, rgb(100, 100, 100) 10px 2px,transparent 11px 2px,transparent 0 3px, rgb(100, 100, 100) 1px 3px,transparent 2px 3px,transparent 3px 3px,transparent 4px 3px,transparent 5px 3px,transparent 6px 3px,transparent 7px 3px,transparent 8px 3px,transparent 9px 3px, rgb(100, 100, 100) 10px 3px,transparent 11px 3px,transparent 0 4px, rgb(100, 100, 100) 1px 4px,transparent 2px 4px,transparent 3px 4px,transparent 4px 4px,transparent 5px 4px,transparent 6px 4px,transparent 7px 4px,transparent 8px 4px,transparent 9px 4px, rgb(100, 100, 100) 10px 4px,transparent 11px 4px,transparent 0 5px, rgb(100, 100, 100) 1px 5px,transparent 2px 5px,transparent 3px 5px,transparent 4px 5px,transparent 5px 5px,transparent 6px 5px,transparent 7px 5px,transparent 8px 5px,transparent 9px 5px, rgb(100, 100, 100) 10px 5px,transparent 11px 5px,transparent 0 6px, rgb(100, 100, 100) 1px 6px,transparent 2px 6px,transparent 3px 6px,transparent 4px 6px,transparent 5px 6px,transparent 6px 6px,transparent 7px 6px,transparent 8px 6px,transparent 9px 6px, rgb(100, 100, 100) 10px 6px,transparent 11px 6px,transparent 0 7px, rgb(100, 100, 100) 1px 7px,transparent 2px 7px,transparent 3px 7px,transparent 4px 7px,transparent 5px 7px,transparent 6px 7px,transparent 7px 7px,transparent 8px 7px,transparent 9px 7px, rgb(100, 100, 100) 10px 7px,transparent 11px 7px,transparent 0 8px, rgb(100, 100, 100) 1px 8px,transparent 2px 8px,transparent 3px 8px,transparent 4px 8px,transparent 5px 8px,transparent 6px 8px,transparent 7px 8px,transparent 8px 8px,transparent 9px 8px, rgb(100, 100, 100) 10px 8px,transparent 11px 8px,transparent 0 9px, rgb(100, 100, 100) 1px 9px,transparent 2px 9px,transparent 3px 9px,transparent 4px 9px,transparent 5px 9px,transparent 6px 9px,transparent 7px 9px,transparent 8px 9px,transparent 9px 9px, rgb(100, 100, 100) 10px 9px,transparent 11px 9px,transparent 0 10px, rgb(100, 100, 100) 1px 10px, rgb(100, 100, 100) 2px 10px, rgb(100, 100, 100) 3px 10px, rgb(100, 100, 100) 4px 10px, rgb(100, 100, 100) 5px 10px, rgb(100, 100, 100) 6px 10px, rgb(100, 100, 100) 7px 10px, rgb(100, 100, 100) 8px 10px, rgb(100, 100, 100) 9px 10px, rgb(100, 100, 100) 10px 10px,transparent 11px 10px,transparent 0 11px,transparent 1px 11px,transparent 2px 11px,transparent 3px 11px,transparent 4px 11px,transparent 5px 11px,transparent 6px 11px,transparent 7px 11px,transparent 8px 11px,transparent 9px 11px,transparent 10px 11px,transparent 11px 11px;
				}
				
				:host #windowAction button.close > span > span {
					box-shadow: transparent 0 0,transparent 1px 0,transparent 2px 0,transparent 3px 0,transparent 4px 0,transparent 5px 0,transparent 6px 0,transparent 7px 0,transparent 8px 0,transparent 9px 0,transparent 10px 0,transparent 11px 0,transparent 0 1px, rgb(0, 0, 0) 1px 1px,transparent 2px 1px,transparent 3px 1px,transparent 4px 1px,transparent 5px 1px,transparent 6px 1px,transparent 7px 1px,transparent 8px 1px,transparent 9px 1px, rgb(0, 0, 0) 10px 1px,transparent 11px 1px,transparent 0 2px,transparent 1px 2px, rgb(0, 0, 0) 2px 2px,transparent 3px 2px,transparent 4px 2px,transparent 5px 2px,transparent 6px 2px,transparent 7px 2px,transparent 8px 2px, rgb(0, 0, 0) 9px 2px,transparent 10px 2px,transparent 11px 2px,transparent 0 3px,transparent 1px 3px,transparent 2px 3px, rgb(0, 0, 0) 3px 3px,transparent 4px 3px,transparent 5px 3px,transparent 6px 3px,transparent 7px 3px, rgb(0, 0, 0) 8px 3px,transparent 9px 3px,transparent 10px 3px,transparent 11px 3px,transparent 0 4px,transparent 1px 4px,transparent 2px 4px,transparent 3px 4px, rgb(0, 0, 0) 4px 4px,transparent 5px 4px,transparent 6px 4px, rgb(0, 0, 0) 7px 4px,transparent 8px 4px,transparent 9px 4px,transparent 10px 4px,transparent 11px 4px,transparent 0 5px,transparent 1px 5px,transparent 2px 5px,transparent 3px 5px,transparent 4px 5px, rgb(0, 0, 0) 5px 5px, rgb(0, 0, 0) 6px 5px,transparent 7px 5px,transparent 8px 5px,transparent 9px 5px,transparent 10px 5px,transparent 11px 5px,transparent 0 6px,transparent 1px 6px,transparent 2px 6px,transparent 3px 6px,transparent 4px 6px, rgb(0, 0, 0) 5px 6px, rgb(0, 0, 0) 6px 6px,transparent 7px 6px,transparent 8px 6px,transparent 9px 6px,transparent 10px 6px,transparent 11px 6px,transparent 0 7px,transparent 1px 7px,transparent 2px 7px,transparent 3px 7px, rgb(0, 0, 0) 4px 7px,transparent 5px 7px,transparent 6px 7px, rgb(0, 0, 0) 7px 7px,transparent 8px 7px,transparent 9px 7px,transparent 10px 7px,transparent 11px 7px,transparent 0 8px,transparent 1px 8px,transparent 2px 8px, rgb(0, 0, 0) 3px 8px,transparent 4px 8px,transparent 5px 8px,transparent 6px 8px,transparent 7px 8px, rgb(0, 0, 0) 8px 8px,transparent 9px 8px,transparent 10px 8px,transparent 11px 8px,transparent 0 9px,transparent 1px 9px, rgb(0, 0, 0) 2px 9px,transparent 3px 9px,transparent 4px 9px,transparent 5px 9px,transparent 6px 9px,transparent 7px 9px,transparent 8px 9px, rgb(0, 0, 0) 9px 9px,transparent 10px 9px,transparent 11px 9px,transparent 0 10px, rgb(0, 0, 0) 1px 10px,transparent 2px 10px,transparent 3px 10px,transparent 4px 10px,transparent 5px 10px,transparent 6px 10px,transparent 7px 10px,transparent 8px 10px,transparent 9px 10px, rgb(0, 0, 0) 10px 10px,transparent 11px 10px,transparent 0 11px,transparent 1px 11px,transparent 2px 11px,transparent 3px 11px,transparent 4px 11px,transparent 5px 11px,transparent 6px 11px,transparent 7px 11px,transparent 8px 11px,transparent 9px 11px,transparent 10px 11px,transparent 11px 11px;
				}
				
				:host #windowAction button.close:hover > span > span {
					box-shadow: transparent 0 0,transparent 1px 0,transparent 2px 0,transparent 3px 0,transparent 4px 0,transparent 5px 0,transparent 6px 0,transparent 7px 0,transparent 8px 0,transparent 9px 0,transparent 10px 0,transparent 11px 0,transparent 0 1px, rgb(255, 255, 255) 1px 1px,transparent 2px 1px,transparent 3px 1px,transparent 4px 1px,transparent 5px 1px,transparent 6px 1px,transparent 7px 1px,transparent 8px 1px,transparent 9px 1px, rgb(255, 255, 255) 10px 1px,transparent 11px 1px,transparent 0 2px,transparent 1px 2px, rgb(255, 255, 255) 2px 2px,transparent 3px 2px,transparent 4px 2px,transparent 5px 2px,transparent 6px 2px,transparent 7px 2px,transparent 8px 2px, rgb(255, 255, 255) 9px 2px,transparent 10px 2px,transparent 11px 2px,transparent 0 3px,transparent 1px 3px,transparent 2px 3px, rgb(255, 255, 255) 3px 3px,transparent 4px 3px,transparent 5px 3px,transparent 6px 3px,transparent 7px 3px, rgb(255, 255, 255) 8px 3px,transparent 9px 3px,transparent 10px 3px,transparent 11px 3px,transparent 0 4px,transparent 1px 4px,transparent 2px 4px,transparent 3px 4px, rgb(255, 255, 255) 4px 4px,transparent 5px 4px,transparent 6px 4px, rgb(255, 255, 255) 7px 4px,transparent 8px 4px,transparent 9px 4px,transparent 10px 4px,transparent 11px 4px,transparent 0 5px,transparent 1px 5px,transparent 2px 5px,transparent 3px 5px,transparent 4px 5px, rgb(255, 255, 255) 5px 5px, rgb(255, 255, 255) 6px 5px,transparent 7px 5px,transparent 8px 5px,transparent 9px 5px,transparent 10px 5px,transparent 11px 5px,transparent 0 6px,transparent 1px 6px,transparent 2px 6px,transparent 3px 6px,transparent 4px 6px, rgb(255, 255, 255) 5px 6px, rgb(255, 255, 255) 6px 6px,transparent 7px 6px,transparent 8px 6px,transparent 9px 6px,transparent 10px 6px,transparent 11px 6px,transparent 0 7px,transparent 1px 7px,transparent 2px 7px,transparent 3px 7px, rgb(255, 255, 255) 4px 7px,transparent 5px 7px,transparent 6px 7px, rgb(255, 255, 255) 7px 7px,transparent 8px 7px,transparent 9px 7px,transparent 10px 7px,transparent 11px 7px,transparent 0 8px,transparent 1px 8px,transparent 2px 8px, rgb(255, 255, 255) 3px 8px,transparent 4px 8px,transparent 5px 8px,transparent 6px 8px,transparent 7px 8px, rgb(255, 255, 255) 8px 8px,transparent 9px 8px,transparent 10px 8px,transparent 11px 8px,transparent 0 9px,transparent 1px 9px, rgb(255, 255, 255) 2px 9px,transparent 3px 9px,transparent 4px 9px,transparent 5px 9px,transparent 6px 9px,transparent 7px 9px,transparent 8px 9px, rgb(255, 255, 255) 9px 9px,transparent 10px 9px,transparent 11px 9px,transparent 0 10px, rgb(255, 255, 255) 1px 10px,transparent 2px 10px,transparent 3px 10px,transparent 4px 10px,transparent 5px 10px,transparent 6px 10px,transparent 7px 10px,transparent 8px 10px,transparent 9px 10px, rgb(255, 255, 255) 10px 10px,transparent 11px 10px,transparent 0 11px,transparent 1px 11px,transparent 2px 11px,transparent 3px 11px,transparent 4px 11px,transparent 5px 11px,transparent 6px 11px,transparent 7px 11px,transparent 8px 11px,transparent 9px 11px,transparent 10px 11px,transparent 11px 11px;
				}
				
				:host #windowAction button > span > span {
					transition: box-shadow 0.2s ease-in-out;
					display: block;
					width: 1px;
					height: 1px;
				}
				
				:host #windowAction button > span {
					display: inline-block;
					width: 12px;
					height: 12px;
				}
				
				:host #windowAction button {
					transition: background 0.2s ease-in-out;
					background: transparent;
					position: relative;
					line-height: 36px;
					padding: 0 12px;
					cursor: pointer;
					border: none;
					height: 100%;
					width: 46px;
					margin: 0;
				}
				
				:host #windowAction button:hover {
					background: rgb(200, 200, 202);
				}
				
				:host #windowAction button.close:hover {
					background: rgb(232, 17, 35);
				}
				
				:host #window.expand #windowAction button.expand, :host #window:not(.expand) #windowAction button.compress {
					display: none;
				}
				
				:host #content {
					width: calc(100% - 20px);
					overflow: auto;
					display: block;
					padding: 10px;
					flex: 1;
				}
				
				:host #footer {
					
				}
				
				:host #resize {
					cursor: nwse-resize;
					position: absolute;
					height: 15px;
					width: 15px;
					bottom: 0;
					right: 0;
				}
			</style>
			<div id="window" style="height: 400px;width: 800px;" on-click="windowClick">
		    	<div id="header">
		    		<div id="dragArea" on-mousedown="move">
		    			<label>[[label]]</label>
		    		</div>
		    		<div id="windowAction">
		    			<button on-click="windowMinimize" id="buttonMinimize" class="minimize"><span><span></span></span></button>
		    			<button on-click="windowCompress" id="buttonCompress" class="compress"><span><span></span></span></button>
		    			<button on-click="windowExpand" id="buttonExpand" class="expand"><span><span></span></span></button>
		    			<button on-click="windowClose" id="buttonClose" class="close"><span><span></span></span></button>
		    		</div>
		    	</div>
		   		<div id="content"></div>
		   		<div id="footer"></div>
		    	<div id="resize" on-mousedown="resize"></div>
		    </div>
		`;
	}
}

customElements.define(WindowElement.is, WindowElement);