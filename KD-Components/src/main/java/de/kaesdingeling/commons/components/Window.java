package de.kaesdingeling.commons.components;

import java.util.ArrayList;
import java.util.List;

import com.google.common.collect.Lists;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasComponents;
import com.vaadin.flow.component.HasSize;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.dependency.JsModule;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.NativeButton;
import com.vaadin.flow.component.polymertemplate.EventHandler;
import com.vaadin.flow.component.polymertemplate.Id;
import com.vaadin.flow.component.polymertemplate.PolymerTemplate;

import de.kaesdingeling.commons.components.data.interfaces.IWindowUpdate;
import de.kaesdingeling.commons.components.data.model.WindowModel;

@Tag("window-overlay")
@JsModule("./KD-Components/Window/Window.js")
public class Window extends PolymerTemplate<WindowModel> implements HasComponents, HasSize, HasStyle {
	private static final long serialVersionUID = 7973503563185641415L;
	
	private List<IWindowUpdate> updateListeners = Lists.newArrayList();
	
	@Id("window")
	protected Div window;
	@Id("windowAction")
	protected Div windowAction;
	@Id("content")
	protected Div content;
	@Id("footer")
	protected Div footer;
	@Id("buttonMinimize")
	protected NativeButton buttonMinimize;
	@Id("buttonCompress")
	protected NativeButton buttonCompress;
	@Id("buttonExpand")
	protected NativeButton buttonExpand;
	@Id("buttonClose")
	protected NativeButton buttonClose;
	
	private Component icon = null;
	private boolean open = false;
	
	public Window() {
		this(true);
	}
	
	public Window(boolean center) {
		super();
		
		if (center) {
			center();
		}
	}
    
    public IWindowUpdate addUpdateListener(IWindowUpdate updateListener) {
    	updateListeners.add(updateListener);
    	return updateListener;
    }
    
    public Window removeUpdateListener(IWindowUpdate updateListener) {
    	updateListeners.remove(updateListener);
    	return this;
    }
    
    public void setLabel(String label) {
    	getModel().setLabel(label);
    }
    
    public String getLabel() {
    	return getModel().getLabel();
    }
    
    public void setIcon(Component icon) {
    	this.icon = icon;
    }
    
    public Component getIcon() {
    	return icon;
    }
    
    @EventHandler
    private void windowMinimize() {
    	setVisible(false);
    	updateListeners.forEach(e -> e.minimize());
    }
    
    @EventHandler
    private void windowCompress() {
    	window.getClassNames().remove("expand");
    	updateListeners.forEach(e -> e.compress());
    }
    
    @EventHandler
    private void windowExpand() {
    	window.getClassNames().add("expand");
    	updateListeners.forEach(e -> e.expand());
    }
    
    @EventHandler
    private void toggleExpand() {
    	if (isExpand()) {
    		windowCompress();
    	} else {
    		windowExpand();
    	}
    }
    
    @EventHandler
    private void windowClose() {
    	close();
    }

    public void center() {
    	getElement().callJsFunction("center");
    }
    
    @Override
    public void add(Component... components) {
    	content.add(components);
    }
    
    @Override
    public void remove(Component... components) {
    	content.remove(components);
    }
    
    public int getComponentCount() {
        return (int) content.getChildren().count();
    }
    
    public Component getComponentAt(int index) {
        if (index < 0) {
            throw new IllegalArgumentException("The 'index' argument should be greater than 0. It was: " + index);
        }
        return content.getChildren().sequential().skip(index).findFirst().orElseThrow(() -> new IllegalArgumentException("The 'index' argument should not be greater than or equals to the number of children components. It was: " + index));
    }
    
    
    public boolean isOpen() {
    	return open;
    }
	
	public void open() {
        if (!isOpen()) {
        	UI ui = getCurrentUI();
            ui.beforeClientResponse(ui, context -> {
                ui.add(this);
                open = true;
            });
        	updateListeners.forEach(e -> e.open());
        }
    }
	
	public void close() {
		if (isOpen()) {
			boolean closeBreak = false;
			
			for (IWindowUpdate iWindowUpdate : updateListeners) {
				if (!iWindowUpdate.close()) {
					closeBreak = true;
					break;
				}
			}
			
			if (!closeBreak) {
				UI ui = getCurrentUI();
				setVisible(false);
				new Thread(() -> {
					try {
						Thread.sleep(200);
						ui.access(() -> {
							ui.remove(this);
				            open = false;
						});
					} catch (Exception e) {
					}
				}).start();
			}
		}
    }
	
	private UI getCurrentUI() {
        UI ui = UI.getCurrent();
        if (ui == null) {
            throw new IllegalStateException("UI instance is not available. "
                    + "It means that you are calling this method "
                    + "out of a normal workflow where it's always implicitely set. "
                    + "That may happen if you call the method from the custom thread without "
                    + "'UI::access' or from tests without proper initialization.");
        }
        return ui;
    }
	
	@Override
	public void setWidth(String width) {
		window.setWidth(width);
	}
	
	@Override
	public void setHeight(String height) {
		window.setHeight(height);
	}
	
	public boolean isExpand() {
		return window.getClassNames().contains("expand");
	}
	
	@Override
	public boolean isVisible() {
		return !getClassNames().contains("hide");
	}
	
	@Override
	public void setVisible(boolean visible) {
		if (isVisible() == !visible) {
			if (visible) {
				getClassNames().remove("hide");
			} else {
				getClassNames().add("hide");
			}
		}
    	updateListeners.forEach(e -> e.visible());
	}
	
	public static List<Window> getAllWindow() {
		List<Window> windows = new ArrayList<Window>();
		UI.getCurrent().getElement().getChildren()
			.filter(i -> i.getComponent().isPresent())
			.filter(i -> i.getComponent().get() instanceof Window)
			.forEach(i -> windows.add((Window) i.getComponent().get()));
		return windows;
	}
}