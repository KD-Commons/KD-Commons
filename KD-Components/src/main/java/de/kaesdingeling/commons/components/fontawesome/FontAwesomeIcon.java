package de.kaesdingeling.commons.components.fontawesome;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.icon.Icon;

import de.kaesdingeling.commons.components.fontawesome.icons.FontAwesomeBrands;
import de.kaesdingeling.commons.components.fontawesome.icons.FontAwesomeRegular;
import de.kaesdingeling.commons.components.fontawesome.icons.FontAwesomeSolid;

/**
 * 
 * @created 16.07.2020 - 14:12:56
 * @author KaesDingeling
 * @version 0.1
 */
@Tag("font-awesome-icon")
@StyleSheet("./frontend/KD-Components/FontAwesome/css/all.min.css")
public class FontAwesomeIcon extends Icon {
	private static final long serialVersionUID = -437800951037517092L;
	
	protected static final String ICON_ATTRIBUTE_NAME = "icon";
	protected static final String ICON_ATTRIBUTE_VALUE = "fontawesome";

	protected static final String CSS_VALUE_FLEX = "flex";
	protected static final String CSS_VALUE_CENTER = "center";
	protected static final String CSS_ATTR_DISPLAY = "display";
	protected static final String CSS_ATTR_ALIGN_ITEMS = "align-items";
	protected static final String CSS_ATTR_JUSTIFY_CONTENT = "justify-content";
	
	/**
	 * 
	 * @param solid
	 */
	public FontAwesomeIcon(FontAwesomeSolid solid) {
		super();
		init();
		
		setParameter(Type.FAS, solid.name());
	}
	
	/**
	 * 
	 * @param regular
	 */
	public FontAwesomeIcon(FontAwesomeRegular regular) {
		super();
		init();
		
		setParameter(Type.FAR, regular.name());
	}
	
	/**
	 * 
	 * @param brands
	 */
	public FontAwesomeIcon(FontAwesomeBrands brands) {
		super();
		init();
		
		setParameter(Type.FAB, brands.name());
	}
	
	/**
	 * 
	 * 
	 * @Created 16.07.2020 - 14:13:06
	 * @author KaesDingeling
	 */
	private void init() {
		getStyle().set(CSS_ATTR_DISPLAY, CSS_VALUE_FLEX);
		getStyle().set(CSS_ATTR_ALIGN_ITEMS, CSS_VALUE_CENTER);
		getStyle().set(CSS_ATTR_JUSTIFY_CONTENT, CSS_VALUE_CENTER);
		
		getElement().setAttribute(ICON_ATTRIBUTE_NAME, ICON_ATTRIBUTE_VALUE);
	}
	
	/**
	 * 
	 * @param type
	 * @param icon
	 * @Created 16.07.2020 - 14:13:08
	 * @author KaesDingeling
	 */
	private void setParameter(Type type, String icon) {
		if (icon.startsWith("_")) {
			icon = icon.substring(1);
		}
		
		getClassNames().add(type.name().toLowerCase());
		getClassNames().add("fa-" + icon.replaceAll("_", "-").toLowerCase());
	}
	
	/**
	 * 
	 * @created 16.07.2020 - 14:13:11
	 * @author KaesDingeling
	 * @version 0.1
	 */
	private static enum Type {
		FA,
		FAL,
		FAS,
		FAR,
		FAB;
	}
}