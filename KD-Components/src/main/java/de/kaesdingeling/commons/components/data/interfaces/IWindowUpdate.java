package de.kaesdingeling.commons.components.data.interfaces;

public interface IWindowUpdate {
	void changes();
	void open();
	void expand();
	void visible();
	void compress();
	void minimize();
	boolean close();
}