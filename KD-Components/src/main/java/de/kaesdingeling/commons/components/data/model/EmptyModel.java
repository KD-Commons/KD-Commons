package de.kaesdingeling.commons.components.data.model;

import com.vaadin.flow.templatemodel.TemplateModel;

public interface EmptyModel extends TemplateModel {
	void setLabel(String time);
	String getLabel();
}