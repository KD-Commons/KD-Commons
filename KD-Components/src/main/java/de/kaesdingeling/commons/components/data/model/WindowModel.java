package de.kaesdingeling.commons.components.data.model;

import com.vaadin.flow.templatemodel.TemplateModel;

public interface WindowModel extends TemplateModel {
	void setLabel(String label);
	String getLabel();
}