function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

async function clickOnButton(ms, id) {
  await sleep(ms);
  document.getElementById(id).click();
}