package de.kaesdingeling.commons.ui.utils.interfaces;

import java.util.Objects;

import com.vaadin.flow.component.Component;

/**
 * 
 * @created 14.06.2020 - 17:22:07
 * @author KaesDingeling
 * @version 0.1
 */
public interface HasCurrency {
	
	/**
	 * 
	 * 
	 * @Created 14.06.2020 - 17:22:05
	 * @author KaesDingeling
	 */
	public void updateCurrency();
	
	/**
	 * 
	 * 
	 * @Created 24.06.2020 - 22:09:05
	 * @author KaesDingeling
	 */
	public default void preUpdateCurrency() {
		if (this instanceof Component) {
			((Component) this).getChildren()
					.filter(Objects::nonNull)
					.filter(c -> c instanceof HasCurrency)
					.map(c -> (HasCurrency) c)
					.forEach(c -> c.preUpdateCurrency());
		}
		
		updateCurrency();
	}
}