package de.kaesdingeling.commons.ui.utils.components;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public class VerticalContentLayout extends VerticalLayout {
	private static final long serialVersionUID = -875158558260069634L;
	
	/**
     * Constructs an empty layout with spacing and padding on by default.
     */
	public VerticalContentLayout() {
    	setStyle();
        setWidth("100%");
        setHeight("100%");
        setSpacing(true);
        setPadding(true);
    }

    /**
     * Convenience constructor to create a layout with the children already
     * inside it.
     *
     * @param children
     *            the items to add to this layout
     * @see #add(Component...)
     */
    public VerticalContentLayout(Component... children) {
        this();
        add(children);
    }
    
    private void setStyle() {
    	getClassNames().add("content");
    }
}