package de.kaesdingeling.commons.ui.utils.components;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.ComponentEventListener;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.html.H3;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

import de.kaesdingeling.commons.ui.utils.Styles;
import de.kaesdingeling.commons.ui.utils.theme.VaadinTheme;
import lombok.Getter;

@StyleSheet("frontend://KD-UI-Utils/style.min.css")
public class ConfirmDialog extends Dialog {
	private static final long serialVersionUID = -8861274155253550733L;

	@Getter
	private Icon icon = null;
	@Getter
	private H3 title;
	@Getter
	private Label message;
	@Getter
	private HorizontalLayout buttons;
	@Getter
	private Button ok;
	@Getter
	private Button cancel;
	
	public ConfirmDialog(String okText, ComponentEventListener<ClickEvent<Button>> okListener, String cancelText, ComponentEventListener<ClickEvent<Button>> cancelListener) {
		this("Are you sure?", okText, okListener, cancelText, cancelListener);
	}
	
	public ConfirmDialog(String message, String okText, ComponentEventListener<ClickEvent<Button>> okListener, String cancelText, ComponentEventListener<ClickEvent<Button>> cancelListener) {
		this("Confirm", message, okText, okListener, cancelText, cancelListener);
	}
	
	public ConfirmDialog(String title, String message, String okText, ComponentEventListener<ClickEvent<Button>> okListener, String cancelText, ComponentEventListener<ClickEvent<Button>> cancelListener) {
		this(VaadinIcon.WARNING.create(), title, message, okText, okListener, cancelText, cancelListener);
	}
	
	public ConfirmDialog(Icon icon, String title, String message, String okText, ComponentEventListener<ClickEvent<Button>> okListener, String cancelText, ComponentEventListener<ClickEvent<Button>> cancelListener) {
		super();
		
		setWidth("300px");
		
		buttons = new HorizontalLayout();
		buttons.setAlignItems(Alignment.END);
		
		this.icon = icon;
		
		if (cancelText != null) {
			cancel = new Button(cancelText, e -> {
				if (cancelListener != null) {
					cancelListener.onComponentEvent(e);
				}
				close();
			});
			buttons.add(cancel);
		}
		
		if (okText != null && okListener != null) {
			ok = new Button(okText, e -> {
				okListener.onComponentEvent(e);
				close();
			});
			VaadinTheme.ERROR.set(ok);
			buttons.add(ok);
		}
		
		this.title = new H3(title);
		this.message = new Label(message);
		
		layout.setMargin(false);
		layout.setClassName(Styles.Dialog.confirm);
		layout.setDefaultHorizontalComponentAlignment(Alignment.CENTER);
		
		setCloseOnEsc(false);
		
		add(this.icon, this.title, this.message, buttons);
		
		open();
	}
}