package de.kaesdingeling.commons.ui.utils;

import com.vaadin.flow.server.VaadinSession;

public class Session {
	public static <T> T get(Class<T> _clazz) {
		return VaadinSession.getCurrent().getAttribute(_clazz);
	}
	
	public static <T> void reset(Class<T> _clazz) {
		set(_clazz, null);
	}
	
	public static <T> void set(Class<T> _clazz, T t) {
		VaadinSession.getCurrent().setAttribute(_clazz, t);
	}
	
	public static <T> void set(String type, T t) {
		VaadinSession.getCurrent().setAttribute(type, t);
	}
	
	@SuppressWarnings("unchecked")
	public static <T> void set(T t) {
		if (t != null) {
			set((Class<T>) t.getClass(), t);
		}
	}
}