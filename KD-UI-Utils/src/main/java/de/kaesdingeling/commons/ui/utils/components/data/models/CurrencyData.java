package de.kaesdingeling.commons.ui.utils.components.data.models;

import java.io.Serializable;
import java.util.Currency;
import java.util.Locale;

import lombok.Builder;
import lombok.Data;

/**
 * 
 * @created 18.06.2020 - 17:48:14
 * @author KaesDingeling
 * @version 0.1
 */
@Data
@Builder
public class CurrencyData implements Serializable {
	private static final long serialVersionUID = 730295784765093721L;
	
	private Locale locale;
	private Currency currency;
}