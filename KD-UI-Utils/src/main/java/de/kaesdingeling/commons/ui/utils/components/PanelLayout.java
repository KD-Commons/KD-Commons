package de.kaesdingeling.commons.ui.utils.components;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;

import de.kaesdingeling.commons.ui.utils.Styles;
import lombok.Getter;

public class PanelLayout extends VerticalLayout {
	private static final long serialVersionUID = 1863619202643719374L;
	
	@Getter
	private Label text;
	
	public PanelLayout(String text) {
		super();
		
		this.text = new Label(text);
		
		add(this.text);
		
		addClassName(Styles.PanelLayout.layout);
	}
}