package de.kaesdingeling.commons.ui.utils.interfaces;

import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.router.AfterNavigationEvent;

public interface IGridLayout<T, W extends Dialog> {
	public W createWindow(T bean);
	public T createBean();
	public void refresh();
	public void build(AfterNavigationEvent event);
}