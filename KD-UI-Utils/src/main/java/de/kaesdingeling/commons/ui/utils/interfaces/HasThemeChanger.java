package de.kaesdingeling.commons.ui.utils.interfaces;

import de.kaesdingeling.commons.ui.utils.components.data.interfaces.IThemeChangerComponent;

/**
 * 
 * @created 14.06.2020 - 18:49:19
 * @author KaesDingeling
 * @version 0.1
 */
public interface HasThemeChanger {
	
	/**
	 * 
	 * @param <TC>
	 * @return
	 * @Created 18.06.2020 - 16:43:43
	 * @author KaesDingeling
	 */
	public <TC extends IThemeChangerComponent> TC getThemeChanger();
}