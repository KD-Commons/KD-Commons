package de.kaesdingeling.commons.ui.utils.components;

import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import com.vaadin.flow.component.HasTheme;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextFieldVariant;

import de.kaesdingeling.commons.ui.utils.UIUtils;
import de.kaesdingeling.commons.ui.utils.components.data.interfaces.ICurrencyChangerComponent;
import de.kaesdingeling.commons.ui.utils.components.data.models.CurrencyData;
import de.kaesdingeling.commons.ui.utils.interfaces.HasCurrency;

/**
 * 
 * @created 14.06.2020 - 19:14:32
 * @author KaesDingeling
 * @version 0.1
 */
public class CurrencyChanger extends Select<CurrencyData> implements HasTheme, ICurrencyChangerComponent {
	private static final long serialVersionUID = 8002989077908619778L;
	
	private boolean updateSilent = false;

	/**
	 * 
	 * @param titleGetter
	 */
	public CurrencyChanger() {
		super();
		
		addThemeName(TextFieldVariant.LUMO_SMALL.getVariantName());
		
		setItemLabelGenerator(i -> i.getCurrency().getSymbol(i.getLocale()) + " " + i.getCurrency().getCurrencyCode());
		
		addValueChangeListener(e -> {
			if (!updateSilent) {
				UIUtils.updateCurrencyForAllSessionUIs(UI.getCurrent(), e.getValue());
			}
		});
	}
	
	/**
	 * 
	 * 
	 * @Created 14.06.2020 - 19:19:23
	 * @author KaesDingeling
	 */
	@Override
	public void refresh() {
		UI ui = UI.getCurrent();
		
		setValueSilent(ui, ui.getSession().getAttribute(CurrencyData.class));
		
		Set<HasCurrency> components = ui.getChildren()
			.filter(Objects::nonNull)
			.filter(c -> c instanceof HasCurrency)
			.map(c -> (HasCurrency) c)
			.collect(Collectors.toSet());
		
		components.addAll(ui.getInternals().getActiveRouterTargetsChain().stream()
			.filter(Objects::nonNull)
			.filter(c -> c instanceof HasCurrency)
			.map(c -> (HasCurrency) c)
			.collect(Collectors.toSet()));
		
		components.forEach(hasLanguage -> hasLanguage.preUpdateCurrency());
	}
	
	/**
	 * 
	 * @param locale
	 * @Created 14.06.2020 - 19:24:13
	 * @author KaesDingeling
	 */
	@Override
	public void setValueSilent(CurrencyData data) {
		setValueSilent(UI.getCurrent(), data);
	}
	
	/**
	 * 
	 * @param ui
	 * @param locale
	 * @Created 14.06.2020 - 19:25:10
	 * @author KaesDingeling
	 */
	@Override
	public void setValueSilent(UI ui, CurrencyData data) {
		updateSilent = true;
		
		setValue(data);
		
		updateSilent = false;
	}
}