package de.kaesdingeling.commons.ui.utils.components;

import de.kaesdingeling.commons.ui.utils.Session;
import de.kaesdingeling.commons.utils.JPAConnector;
import de.kaesdingeling.commons.utils.database.abstracts.AbstractEntity;
import de.kaesdingeling.commons.utils.database.abstracts.AbstractWithCreatedEntity;

public abstract class BinderDBDialog<T extends AbstractEntity> extends BinderDialog<T, BinderDBDialog<T>> {
	private static final long serialVersionUID = -4686756488193444437L;
	
	@Override
	public void open() {
		if (changesSaved) {
			binder.addValueChangeListener(e -> {
				saveValid(save, binder);
				if (changesSaved) {
					changesSaved = false;
				}
			});
		}
		if (remove != null && binder.getBean() != null && binder.getBean().isNew()) {
			remove.setVisible(false);
		}
		saveValid(save, binder);
		super.open();
	}
	
	public void save() {
		if (binder.getBean() != null) {
			if (binder.getBean().isNew()) {
				persist();
			} else {
				merge();
			}
			close();
		}
	}
	
	public void merge() {
		if (binder.getBean() != null) {
			binder.setBean(Session.get(JPAConnector.class).merge(binder.getBean()));
			changesSaved = true;
		}
	}
	
	public void persist() {
		if (binder.getBean() != null) {
			if (binder.getBean() instanceof AbstractWithCreatedEntity) {
				((AbstractWithCreatedEntity) binder.getBean()).created();
			}
			binder.setBean(Session.get(JPAConnector.class).persist(binder.getBean()));
			changesSaved = true;
		}
	}
	
	public void delete() {
		if (binder.getBean() != null) {
			new ConfirmDialog("Löschen", "Möchten Sie diesen Eintrag wirklich Löschen?", "Ja", e -> {
				T t = Session.get(JPAConnector.class).remove(binder.getBean());
				
				changesSaved = true;
				
				if (t != null) {
					new ConfirmDialog("Löschen", "Löschen nicht möglich!", "Ok", e1 -> {
						
					}, null, null).open();
				} else {
					close();
				}
			}, "Nein", e -> {
				
			}).open();
		}
	}
}