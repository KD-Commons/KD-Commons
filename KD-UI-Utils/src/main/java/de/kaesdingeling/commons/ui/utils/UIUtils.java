package de.kaesdingeling.commons.ui.utils;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Currency;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import org.apache.commons.lang3.StringUtils;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.Grid.SelectionMode;
import com.vaadin.flow.component.page.PendingJavaScriptResult;
import com.vaadin.flow.server.Command;
import com.vaadin.flow.server.StreamResource;
import com.vaadin.flow.theme.lumo.Lumo;

import de.kaesdingeling.commons.ui.utils.components.Dialog;
import de.kaesdingeling.commons.ui.utils.components.data.interfaces.ICurrencyChangerComponent;
import de.kaesdingeling.commons.ui.utils.components.data.interfaces.ILanguageChangerComponent;
import de.kaesdingeling.commons.ui.utils.components.data.interfaces.IThemeChangerComponent;
import de.kaesdingeling.commons.ui.utils.components.data.models.CurrencyData;
import de.kaesdingeling.commons.ui.utils.constants.KD_UI_CONSTANTS;
import de.kaesdingeling.commons.ui.utils.interfaces.HasCurrencyChanger;
import de.kaesdingeling.commons.ui.utils.interfaces.HasLanguageChanger;
import de.kaesdingeling.commons.ui.utils.interfaces.HasThemeChanger;
import de.kaesdingeling.commons.ui.utils.interfaces.IDialogListener;
import de.kaesdingeling.commons.ui.utils.interfaces.IGridWithWindowEditor;
import de.kaesdingeling.commons.utils.Utils;
import de.kaesdingeling.commons.utils.database.abstracts.AbstractEntity;
import dev.mett.vaadin.tooltip.Tooltips;
import dev.mett.vaadin.tooltip.config.TC_HIDE_ON_CLICK;
import dev.mett.vaadin.tooltip.config.TooltipConfiguration;

public class UIUtils {
	public static <T, D extends Dialog> Grid<T> gridWithWindowEditor(Grid<T> grid, IGridWithWindowEditor<T, D> gridRefresh) {
		grid.setSelectionMode(SelectionMode.SINGLE);
		grid.addSelectionListener(e -> {
			if (e.getAllSelectedItems().size() > 0) {
				registerWindow(e.getFirstSelectedItem().get(), gridRefresh);
				grid.deselectAll();
			}
		});
		
		return grid;
	}
	
	public static <T, D extends Dialog> D registerWindow(T t, IGridWithWindowEditor<T, D> gridRefresh) {
		D d = gridRefresh.openWindow(t);
		d.addDialogListener(new IDialogListener() {
			@Override
			public void open() {
				
			}
			
			@Override
			public void close() {
				gridRefresh.refresh();
			}
		});
		d.open();
		return d;
	}
	
	public static <T> void singleThread(UI ui, final Command command) {
		ui.access(command);
	}
	
	public static <T> void singleThread(UI ui, Grid<T> grid) {
		Thread thread = new Thread(() -> {
			try {
				grid.getDataProvider().refreshAll();
				ui.access(() -> { });
			} catch (Exception e2) {
			}
		});
		thread.start();
	}
	
	public static <T extends AbstractEntity> List<T> filterDBListForComboBox(T t, List<T> dbList) {
		if (t != null && dbList != null && !t.isNew()) {
			for (int i = 0; i < dbList.size(); i++) {
				if (dbList.get(i).getId() == t.getId()) {
					dbList.remove(dbList.get(i));
					dbList.add(i, t);
					break;
				}
			}
		}
		return dbList;
	}
	
	public static StreamResource getStreamResource(Class<?> _clazz, String filePath, String fileName) throws IOException {
		return getStreamResource(fileName, Utils.getBytes(_clazz, filePath, fileName));
	}
	
	public static StreamResource getStreamResource(String fileName, byte[] bytes) {
		return new StreamResource(fileName, () -> new ByteArrayInputStream(bytes));
	}
	
	public static StreamResource getStreamResource(File file) throws IOException {
		return getStreamResource(file.getName(), Files.readAllBytes(file.toPath()));
	}
	
	public static StreamResource getStreamResource(String fileLink) throws IOException {
		return getStreamResource(new File(fileLink));
	}
	
	/**
	 * 
	 * @param ui
	 * @param theme
	 * @Created 21.05.2020 - 00:40:42
	 * @author KaesDingeling
	 */
	public static void updateThemeForAllSessionUIs(UI ui, String theme) {
		ui.getSession().setAttribute(KD_UI_CONSTANTS.THEME_CHECK, theme);
		ui.getSession().getUIs().forEach(browserTab -> browserTab.access(() -> setThemeAndUpdateUI(browserTab, theme)));
	}
	
	/**
	 * 
	 * @param ui
	 * @param theme
	 * @Created 21.05.2020 - 00:49:35
	 * @author KaesDingeling
	 */
	public static void setThemeAndUpdateUI(UI ui, String theme) {
		ui.getSession().setAttribute(KD_UI_CONSTANTS.THEME_CHECK, theme);
		ui.getElement().setAttribute(KD_UI_CONSTANTS.THEME, theme);
		ui.getChildren()
				.filter(Objects::nonNull)
				.filter(view -> view instanceof HasThemeChanger)
				.filter(Objects::nonNull)
				.map(view -> (HasThemeChanger) view)
				.filter(Objects::nonNull)
				.map(view -> view.getThemeChanger())
				.filter(Objects::nonNull)
				.map(view -> (IThemeChangerComponent) view)
				.forEach(changer -> changer.refresh());
	}
	
	/**
	 * 
	 * @param ui
	 * @param locale
	 * @Created 14.06.2020 - 19:17:31
	 * @author KaesDingeling
	 */
	public static void updateLanguageForAllSessionUIs(UI ui, Locale locale) {
		ui.getSession().getUIs().forEach(browserTab -> browserTab.access(() -> setLanguageAndUpdateUI(browserTab, locale)));
	}
	
	/**
	 * 
	 * @param ui
	 * @param locale
	 * @Created 14.06.2020 - 19:19:00
	 * @author KaesDingeling
	 */
	public static void setLanguageAndUpdateUI(UI ui, Locale locale) {
		ui.getSession().setAttribute(KD_UI_CONSTANTS.LANGUAGE, locale);
		ui.setLocale(locale);
		ui.getChildren()
				.filter(Objects::nonNull)
				.filter(view -> view instanceof HasLanguageChanger)
				.filter(Objects::nonNull)
				.map(view -> (HasLanguageChanger) view)
				.filter(Objects::nonNull)
				.map(view -> view.getLanguageChanger())
				.filter(Objects::nonNull)
				.map(view -> (ILanguageChangerComponent) view)
				.forEach(changer -> changer.refresh());
	}
	
	/**
	 * 
	 * @param ui
	 * @param data
	 * @Created 14.06.2020 - 19:17:31
	 * @author KaesDingeling
	 */
	public static void updateCurrencyForAllSessionUIs(UI ui, CurrencyData data) {
		ui.getSession().getUIs().forEach(browserTab -> browserTab.access(() -> setCurrencyAndUpdateUI(browserTab, data)));
	}
	
	/**
	 * 
	 * @param ui
	 * @param data
	 * @Created 14.06.2020 - 19:19:00
	 * @author KaesDingeling
	 */
	public static void setCurrencyAndUpdateUI(UI ui, CurrencyData data) {
		ui.getSession().setAttribute(CurrencyData.class, data);
		ui.getChildren()
				.filter(Objects::nonNull)
				.filter(view -> view instanceof HasCurrencyChanger)
				.filter(Objects::nonNull)
				.map(view -> (HasCurrencyChanger) view)
				.filter(Objects::nonNull)
				.map(view -> view.getCurrencyChanger())
				.filter(Objects::nonNull)
				.map(view -> (ICurrencyChangerComponent) view)
				.forEach(changer -> changer.refresh());
	}
	
	/**
	 * 
	 * 
	 * @Created 14.06.2020 - 19:08:10
	 * @author KaesDingeling
	 */
	public static void checkOSTheme() {
		UI ui = UI.getCurrent();
		
		Object value = ui.getSession().getAttribute(KD_UI_CONSTANTS.THEME_CHECK);
		
		if (value == null) {
			PendingJavaScriptResult jsResult = ui.getPage().executeJs("return (window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches);");
			
			jsResult.then(Boolean.class, i -> {
				if (i) {
					setThemeAndUpdateUI(ui, Lumo.DARK);
				} else {
					setThemeAndUpdateUI(ui, Lumo.LIGHT);
				}
			});
		} else if (StringUtils.equals(String.valueOf(value), Lumo.DARK)) {
			setThemeAndUpdateUI(ui, Lumo.DARK);
		} else if (StringUtils.equals(String.valueOf(value), Lumo.LIGHT)) {
			setThemeAndUpdateUI(ui, Lumo.LIGHT);
		}
	}
	
	/**
	 * 
	 * 
	 * @Created 14.06.2020 - 19:29:37
	 * @author KaesDingeling
	 */
	public static void checkOSLanguage() {
		UI ui = UI.getCurrent();
		
		Object value = ui.getSession().getAttribute(KD_UI_CONSTANTS.LANGUAGE);
		
		if (value == null || !(value instanceof Locale)) {
			value = ui.getLocale();
		}
		
		setLanguageAndUpdateUI(ui, (Locale) value);
	}
	
	/**
	 * 
	 * 
	 * @Created 14.06.2020 - 19:29:37
	 * @author KaesDingeling
	 */
	public static void checkOSCurrency() {
		UI ui = UI.getCurrent();
		
		CurrencyData currency = ui.getSession().getAttribute(CurrencyData.class);
		
		if (currency == null) {
			Object value = ui.getSession().getAttribute(KD_UI_CONSTANTS.LANGUAGE);
			
			if (value == null || !(value instanceof Locale)) {
				value = ui.getLocale();
			}
			
			setCurrencyAndUpdateUI(ui, CurrencyData.builder()
					.locale((Locale) value)
					.currency(Currency.getInstance((Locale) value))
					.build());
		}
	}
	
	/**
	 * 
	 * @param <C>
	 * @param c
	 * @param toolTip
	 * @Created 09.07.2020 - 22:18:00
	 * @author KaesDingeling
	 */
	public static <C extends Component & HasStyle> void setToolTip(final C c, final String toolTip) {
		if (StringUtils.isNotBlank(toolTip)) {
			TooltipConfiguration config = new TooltipConfiguration(toolTip);
			
			config.setArrow(true);
			config.setHideOnClick(TC_HIDE_ON_CLICK.FALSE);
			
			Tooltips.getCurrent().setTooltip(c, config);
		} else {
			removeToolTip(c);
		}
	}
	
	/**
	 * 
	 * @param <C>
	 * @param c
	 * @Created 09.07.2020 - 22:18:02
	 * @author KaesDingeling
	 */
	public static <C extends Component & HasStyle> void removeToolTip(final C c) {
		final Tooltips toolTips = Tooltips.getCurrent();
		
		toolTips.hideTooltip(c);
		toolTips.removeTooltip(c);
	}
}