package de.kaesdingeling.commons.ui.utils;

import java.util.regex.Pattern;

import com.vaadin.flow.data.binder.ValidationResult;
import com.vaadin.flow.data.binder.Validator;
import com.vaadin.flow.data.binder.ValueContext;

public class UIValidate {
	private static Pattern moneyValidatorPattern = Pattern.compile("^\\d+(\\.|\\,)\\d{2}$");
	
	public static Validator<String> moneyValidator(String errorMessage) {
		return new Validator<String>() {
			private static final long serialVersionUID = -432119461423095747L;

			@Override
			public ValidationResult apply(String value, ValueContext context) {
				try {
					Double convertetValue = Double.valueOf(value);
					if (convertetValue != null) {
						return ValidationResult.ok();
					}
				} catch (Exception e) {
					
				}
				if (moneyValidatorPattern.matcher(value).matches()) {
					return ValidationResult.ok();
				} else {
					return ValidationResult.error(errorMessage);
				}
			}
		};
	}
	
	public static Validator<String> lengthValidator(String errorMessage, int min, int max) {
		return new Validator<String>() {
			private static final long serialVersionUID = 2802513848265705151L;

			@Override
			public ValidationResult apply(String value, ValueContext context) {
				if (value != null && value.length() >= min && value.length() <= max) {
					return ValidationResult.ok();
				} else {
					return ValidationResult.error(errorMessage);
				}
			}
		};
	}
}