package de.kaesdingeling.commons.ui.utils.interfaces;

import com.vaadin.flow.component.dialog.Dialog;

public interface IGridWithWindowEditor<T, D extends Dialog> {
	public void refresh();
	public D openWindow(T t);
}