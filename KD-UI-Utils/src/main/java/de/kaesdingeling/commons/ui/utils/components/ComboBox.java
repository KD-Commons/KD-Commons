package de.kaesdingeling.commons.ui.utils.components;

import com.vaadin.flow.component.KeyNotifier;

public class ComboBox<T> extends com.vaadin.flow.component.combobox.ComboBox<T> implements KeyNotifier {
	private static final long serialVersionUID = 6730469226736832806L;
}