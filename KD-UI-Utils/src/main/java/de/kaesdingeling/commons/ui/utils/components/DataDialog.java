package de.kaesdingeling.commons.ui.utils.components;

import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;

public abstract class DataDialog<T, W extends DataDialog<T, W>> extends BinderDialog<T, W> {
	private static final long serialVersionUID = -5924072265503359156L;
	
	protected HorizontalLayout footer = null;
	protected Button discard = null;
	
	public void withDiscardButton() {
		checkFooter();
		
		discard = new Button("Discard", e -> {
			close();
		});
		
		footer.add(discard);
	}
	
	public void withSaveButton() {
		withSaveButton("Save");
	}
	
	public void withSaveButton(String text) {
		checkFooter();
		
		setSave(new Button(text));
		
		footer.add(save);
	}
	
	public void checkFooter() {
		if (footer == null) {
			footer = new HorizontalLayout();
			footer.setAlignItems(Alignment.CENTER);
		}
		
		add(footer);
	}
}