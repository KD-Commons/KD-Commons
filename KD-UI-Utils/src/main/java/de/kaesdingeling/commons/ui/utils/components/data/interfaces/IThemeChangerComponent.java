package de.kaesdingeling.commons.ui.utils.components.data.interfaces;

import com.vaadin.flow.component.UI;

/**
 * 
 * @created 18.06.2020 - 16:38:14
 * @author KaesDingeling
 * @version 0.1
 */
public interface IThemeChangerComponent {
	
	/**
	 * 
	 * 
	 * @Created 18.06.2020 - 16:39:27
	 * @author KaesDingeling
	 */
	public void refresh();
	
	/**
	 * 
	 * 
	 * @Created 18.06.2020 - 16:43:13
	 * @author KaesDingeling
	 */
	public void setThemeDark();
	
	/**
	 * 
	 * 
	 * @Created 18.06.2020 - 16:43:04
	 * @author KaesDingeling
	 */
	public void setThemeLight();
	
	/**
	 * 
	 * 
	 * @Created 18.06.2020 - 16:42:55
	 * @author KaesDingeling
	 */
	public void toggleTheme();
	
	/**
	 * 
	 * @return
	 * @Created 18.06.2020 - 16:42:42
	 * @author KaesDingeling
	 */
	public boolean isDark();
	
	/**
	 * 
	 * @param ui
	 * @return
	 * @Created 18.06.2020 - 16:42:37
	 * @author KaesDingeling
	 */
	public boolean isDark(UI ui);
}