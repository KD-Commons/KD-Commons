package de.kaesdingeling.commons.ui.utils.components;

import com.vaadin.flow.component.Tag;
import com.vaadin.flow.component.html.Div;

/**
 * 
 * @created 14.06.2020 - 22:07:25
 * @author KaesDingeling
 * @version 0.1
 */
@Tag("p")
public class P extends Div {
	private static final long serialVersionUID = 1895519946344798673L;

}