package de.kaesdingeling.commons.ui.utils.components;

import java.util.function.Consumer;

import org.apache.commons.lang3.StringUtils;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.button.ButtonVariant;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.theme.lumo.Lumo;

import de.kaesdingeling.commons.ui.utils.UIUtils;
import de.kaesdingeling.commons.ui.utils.components.data.interfaces.IThemeChangerComponent;
import de.kaesdingeling.commons.ui.utils.constants.KD_UI_CONSTANTS;

/**
 * 
 * @created 14.06.2020 - 18:47:33
 * @author KaesDingeling
 * @version 0.1
 */
public class ThemeChanger extends Button implements IThemeChangerComponent {
	private static final long serialVersionUID = -6138791754775981896L;
	
	private Consumer<ThemeChanger> updateListener;
	
	/**
	 * 
	 * @param updateListener
	 */
	public ThemeChanger(Consumer<ThemeChanger> updateListener) {
		super();
		
		this.updateListener = updateListener;
		
		setIcon(VaadinIcon.PALETE.create());
		
		addThemeVariants(ButtonVariant.LUMO_SMALL);
		
		addClickListener(e -> {
			if (isDark()) {
				setThemeLight();
			} else {
				setThemeDark();
			}
		});
	}
	
	/**
	 * 
	 * 
	 * @Created 14.06.2020 - 18:57:30
	 * @author KaesDingeling
	 */
	@Override
	public void refresh() {
		if (updateListener != null) {
			updateListener.accept(this);
		}
	}
	
	/**
	 * 
	 * 
	 * @Created 14.06.2020 - 19:03:15
	 * @author KaesDingeling
	 */
	@Override
	public void setThemeDark() {
		UI ui = UI.getCurrent();
		
		if (!isDark(ui)) {
			UIUtils.updateThemeForAllSessionUIs(ui, Lumo.DARK);
		}
	}
	
	/**
	 * 
	 * 
	 * @Created 14.06.2020 - 19:03:58
	 * @author KaesDingeling
	 */
	@Override
	public void setThemeLight() {
		UI ui = UI.getCurrent();
		
		if (isDark(ui)) {
			UIUtils.updateThemeForAllSessionUIs(ui, Lumo.LIGHT);
		}
	}
	
	/**
	 * 
	 * 
	 * @Created 14.06.2020 - 19:03:46
	 * @author KaesDingeling
	 */
	@Override
	public void toggleTheme() {
		UI ui = UI.getCurrent();
		
		if (isDark(ui)) {
			UIUtils.updateThemeForAllSessionUIs(ui, Lumo.LIGHT);
		} else {
			UIUtils.updateThemeForAllSessionUIs(ui, Lumo.DARK);
		}
	}
	
	/**
	 * 
	 * @return
	 * @Created 14.06.2020 - 19:03:25
	 * @author KaesDingeling
	 */
	@Override
	public boolean isDark() {
		return isDark(UI.getCurrent());
	}
	
	/**
	 * 
	 * @param ui
	 * @return
	 * @Created 14.06.2020 - 19:02:54
	 * @author KaesDingeling
	 */
	@Override
	public boolean isDark(UI ui) {
		return StringUtils.equals(String.valueOf(ui.getSession().getAttribute(KD_UI_CONSTANTS.THEME_CHECK)), Lumo.DARK);
	}
}