package de.kaesdingeling.commons.ui.utils.components.data.interfaces;

import com.vaadin.flow.component.UI;

import de.kaesdingeling.commons.ui.utils.components.data.models.CurrencyData;

/**
 * 
 * @created 18.06.2020 - 16:38:14
 * @author KaesDingeling
 * @version 0.1
 */
public interface ICurrencyChangerComponent {
	
	/**
	 * 
	 * 
	 * @Created 18.06.2020 - 16:39:27
	 * @author KaesDingeling
	 */
	public void refresh();
	
	
	/**
	 * 
	 * @param ata
	 * @Created 18.06.2020 - 16:39:46
	 * @author KaesDingeling
	 */
	public void setValueSilent(CurrencyData data);
	
	/**
	 * 
	 * @param ui
	 * @param data
	 * @Created 18.06.2020 - 16:39:56
	 * @author KaesDingeling
	 */
	public void setValueSilent(UI ui, CurrencyData data);
}