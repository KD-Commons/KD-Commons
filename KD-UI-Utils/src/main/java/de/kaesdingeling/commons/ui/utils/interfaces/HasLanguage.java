package de.kaesdingeling.commons.ui.utils.interfaces;

import com.vaadin.flow.i18n.LocaleChangeEvent;
import com.vaadin.flow.i18n.LocaleChangeObserver;

/**
 * 
 * @created 14.06.2020 - 17:22:07
 * @author KaesDingeling
 * @version 0.1
 */
public interface HasLanguage extends LocaleChangeObserver {
	
	/**
	 * 
	 * 
	 * @Created 14.06.2020 - 17:22:05
	 * @author KaesDingeling
	 */
	public void updateLanguages();
	
	@Override
	public default void localeChange(LocaleChangeEvent event) {
		updateLanguages();
	}
}