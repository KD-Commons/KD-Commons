package de.kaesdingeling.commons.ui.utils.language.interfaces;

import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import com.vaadin.flow.i18n.I18NProvider;

import de.kaesdingeling.commons.utils.language.interfaces.ILanguageBundle;
import de.kaesdingeling.commons.utils.language.interfaces.ILanguageService;

/**
 * 
 * @created 10.07.2020 - 14:27:22
 * @author KaesDingeling
 * @version 0.1
 */
public interface IUILanguageService extends ILanguageService, I18NProvider {

	@Override
	public default List<Locale> getProvidedLocales() {
		return getBundles()
				.stream()
				.map(ILanguageBundle::getPrimaryLocale)
				.collect(Collectors.toList());
	}
}