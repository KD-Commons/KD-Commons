package de.kaesdingeling.commons.ui.utils.components;

import java.util.ArrayList;
import java.util.List;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.HasStyle;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.HtmlImport;
import com.vaadin.flow.component.dependency.StyleSheet;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.data.binder.Binder;

import de.kaesdingeling.commons.ui.utils.Styles;
import de.kaesdingeling.commons.ui.utils.interfaces.IDialogListener;
import de.kaesdingeling.commons.ui.utils.theme.VaadinTheme;
import lombok.Getter;

@StyleSheet("frontend://KD-UI-Utils/style.min.css")
@HtmlImport("frontend://KD-UI-Utils/Components/Dialog.html")
public class Dialog extends com.vaadin.flow.component.dialog.Dialog implements HasStyle {
	private static final long serialVersionUID = -6983353245929066736L;
	
	@Getter
	protected VerticalLayout layout = new VerticalLayout();
	private List<IDialogListener> listeners = new ArrayList<IDialogListener>();
	
	public Dialog() {
		super();
		
		layout.setMargin(true);
		layout.addClassName(Styles.Dialog.layout);
		
		super.add(layout);
	}
	
	@Override
    public void add(Component... components) {
		layout.add(components);
    }

    @Override
    public void remove(Component... components) {
    	layout.remove(components);
    }

    @Override
    public void removeAll() {
    	layout.removeAll();
    }
	
	@Override
	public void open() {
		listeners.forEach(e -> e.open());
		setCloseOnOutsideClick(false);
		super.open();
	}
	
	public IDialogListener addDialogListener(IDialogListener listener) {
		listeners.add(listener);
		return listener;
	}
	
	public void removeDialogListener(IDialogListener listener) {
		listeners.remove(listener);
	}
	
	@Override
	public void close() {
		listeners.forEach(e -> e.close());
		super.close();
	}
	
	public void saveValid(Button save, Binder<?> binder) {
		if (save != null) {
			if (binder.isValid()) {
				VaadinTheme.SUCCESS.set(save);
			} else {
				VaadinTheme.SUCCESS.setWithoutPrimary(save);
			}
		}
	}
}