package de.kaesdingeling.commons.ui.utils.interfaces;

/**
 * 
 * @created 18.06.2020 - 16:44:01
 * @author KaesDingeling
 * @version 0.1
 */
public interface IBinderDialog {
	
	/**
	 * 
	 * 
	 * @Created 18.06.2020 - 16:44:04
	 * @author KaesDingeling
	 */
	public void save();
	
	/**
	 * 
	 * 
	 * @Created 18.06.2020 - 16:44:06
	 * @author KaesDingeling
	 */
	public void delete();
}