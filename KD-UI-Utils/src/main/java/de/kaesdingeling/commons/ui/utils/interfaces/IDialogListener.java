package de.kaesdingeling.commons.ui.utils.interfaces;

public interface IDialogListener {
	public void open();
	public void close();
}