package de.kaesdingeling.commons.ui.utils;

import java.io.ByteArrayInputStream;

import org.vaadin.olli.FileDownloadWrapper;

import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.server.StreamResource;

import lombok.Getter;
import lombok.Setter;

/**
 * 
 * @created 07.07.2020 - 15:15:54
 * @author KaesDingeling
 * @version 0.1
 */
@Getter
public class FileDownloader {

	@Setter
	private UI ui = null;
	@Setter
	private String fileName = null;
	@Setter
	private byte[] content = null;
	private Button button;
	private FileDownloadWrapper wrapper;
	
	@Setter
	private Runnable cleanUpListener;
	
	/**
	 * 
	 * @param fileName
	 * @return
	 * @Created 07.07.2020 - 15:39:12
	 * @author KaesDingeling
	 */
	public static FileDownloader get(String fileName) {
		return get(fileName, null);
	}
	
	/**
	 * 
	 * @param fileName
	 * @param content
	 * @return
	 * @Created 07.07.2020 - 15:39:09
	 * @author KaesDingeling
	 */
	public static FileDownloader get(String fileName, byte[] content) {
		return get(UI.getCurrent(), fileName, content);
	}
	
	/**
	 * 
	 * @param ui
	 * @param fileName
	 * @return
	 * @Created 07.07.2020 - 15:39:36
	 * @author KaesDingeling
	 */
	public static FileDownloader get(UI ui, String fileName) {
		return get(ui, fileName, null);
	}
	
	/**
	 * 
	 * @param ui
	 * @param fileName
	 * @param content
	 * @return
	 * @Created 07.07.2020 - 15:39:39
	 * @author KaesDingeling
	 */
	public static FileDownloader get(UI ui, String fileName, byte[] content) {
		return new FileDownloader(ui, fileName, content);
	}


	/**
	 * 
	 * @param ui
	 * @param fileName
	 * @param content
	 */
	public FileDownloader(UI ui, String fileName, byte[] content) {
		this.ui = ui;
		this.fileName = fileName;
		this.content = content;
		
		button = new Button("Download");
		
		wrapper = new FileDownloadWrapper(new StreamResource(getFileName(), () -> new ByteArrayInputStream(getContent())));
		wrapper.wrapComponent(button);
		wrapper.getElement().setAttribute("style", "position:fixed;left:-100px;top:-100px;visibility:hidden;");
		
		ui.add(wrapper);
	}
	
	/**
	 * 
	 * 
	 * @Created 07.07.2020 - 15:17:38
	 * @author KaesDingeling
	 */
	public void download() {
		getWrapper().setResource(new StreamResource(getFileName(), () -> new ByteArrayInputStream(getContent())));

		getButton().clickInClient();
		
		if (getUi() != null) {
			new Thread(() -> {
				try {
					Thread.sleep(1000);
					ui.access(() -> cleanUp());
				} catch (Exception e) {
				}
			}).start();
		}
	}
	
	/**
	 * 
	 * 
	 * @Created 07.07.2020 - 15:17:54
	 * @author KaesDingeling
	 */
	public void cleanUp() {
		cleanUp(getUi());
	}
	
	/**
	 * 
	 * @param ui
	 * @Created 07.07.2020 - 16:13:01
	 * @author KaesDingeling
	 */
	public void cleanUp(UI ui) {
		if (ui == null) {
			executeCleanUp();
		} else {
			ui.access(() -> executeCleanUp());
		}
	}
	
	/**
	 * 
	 * 
	 * @Created 07.07.2020 - 16:13:16
	 * @author KaesDingeling
	 */
	private void executeCleanUp() {
		if (getWrapper() != null && ui != null) {
			ui.remove(getWrapper());
		}
		
		if (getCleanUpListener() != null) {
			getCleanUpListener().run();
		}
	}
}