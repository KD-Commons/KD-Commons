package de.kaesdingeling.commons.ui.utils;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import com.vaadin.flow.data.binder.Result;
import com.vaadin.flow.data.binder.ValueContext;
import com.vaadin.flow.data.converter.Converter;

import de.kaesdingeling.commons.utils.Utils;

public class UIConverter {
	public static Converter<String, Double> moneyConverter(String errorMessage, int decimalPlaces) {
		return new Converter<String, Double>() {
			private static final long serialVersionUID = 1009233295906338545L;

			@Override
			public Result<Double> convertToModel(String value, ValueContext context) {
				try {
					if (value.contains(",")) {
						value = value.replaceAll(",", ".");
					} else if (!value.contains(".")) {
						value += ".";
						
						for (int i = 0; i < decimalPlaces; i++) {
							value += "0";
						}
					}
					return Result.ok(Double.valueOf(value));
				} catch (Exception e) {
					return Result.error(errorMessage);
				}
			}

			@Override
			public String convertToPresentation(Double value, ValueContext context) {
				return Utils.formatMoney(value, context.getLocale().get(), false);
			}
		};
	}
	
	public static Converter<LocalDate, Long> dateConverter() {
		return new Converter<LocalDate, Long>() {
			private static final long serialVersionUID = 6819740269056240590L;

			@Override
			public Result<Long> convertToModel(LocalDate value, ValueContext context) {
				return Result.ok(java.sql.Date.valueOf(value).getTime());
			}

			@Override
			public LocalDate convertToPresentation(Long value, ValueContext context) {
				return new Date(value).toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			}
		};
	}
}