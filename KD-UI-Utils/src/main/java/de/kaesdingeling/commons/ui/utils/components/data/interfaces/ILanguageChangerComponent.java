package de.kaesdingeling.commons.ui.utils.components.data.interfaces;

import java.util.Locale;

import com.vaadin.flow.component.UI;

/**
 * 
 * @created 18.06.2020 - 16:38:14
 * @author KaesDingeling
 * @version 0.1
 */
public interface ILanguageChangerComponent {
	
	/**
	 * 
	 * 
	 * @Created 18.06.2020 - 16:39:27
	 * @author KaesDingeling
	 */
	public void refresh();
	
	
	/**
	 * 
	 * @param locale
	 * @Created 18.06.2020 - 16:39:46
	 * @author KaesDingeling
	 */
	public void setValueSilent(Locale locale);
	
	/**
	 * 
	 * @param ui
	 * @param locale
	 * @Created 18.06.2020 - 16:39:56
	 * @author KaesDingeling
	 */
	public void setValueSilent(UI ui, Locale locale);
}