package de.kaesdingeling.commons.ui.utils;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.ParentLayout;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RoutePrefix;
import com.vaadin.flow.router.RouterLayout;

public class UINavigate {
	public static <T extends Component> void navigate(Class<T> navigationTarget) {
		String url = buildUrl(navigationTarget);
		UI.getCurrent().navigate(url);
	}
	
	public static <T extends Component> void reRoute(BeforeEnterEvent event, Class<T> navigationTarget) {
		event.rerouteTo(buildUrl(navigationTarget));
	}
	
	public static <T extends Component> String buildUrl(Class<T> navigationTarget) {
		String url = null;
		
		if (navigationTarget != null) {
			Route route = navigationTarget.getAnnotation(Route.class);
			
			if (route != null) {
				String cacheUrl = route.value();
				
				if (cacheUrl != null) {
					url = cacheUrl;
				}
				
				if (route.layout() != null) {
					cacheUrl = getUrl(route.layout());
					
					if (cacheUrl != null) {
						if (url == null) {
							url = cacheUrl;
						} else {
							url = cacheUrl + "/" + url;
						}
					}
				}
			}
		}
		
		if (url == null) {
			url = "";
		}
		
		return url;
	}
	
	private static <T extends RouterLayout> String getUrl(Class<T> navigationTarget) {
		String url = null;
		
		if (navigationTarget != null) {
			RoutePrefix routePrefix = navigationTarget.getAnnotation(RoutePrefix.class);
			ParentLayout parentLayout = navigationTarget.getAnnotation(ParentLayout.class);
			
			if (routePrefix != null) {
				String cacheUrl = routePrefix.value();
				
				if (cacheUrl != null) {
					url = cacheUrl;
				}
			}
			
			if (parentLayout != null && parentLayout.value() != null) {
				String cacheUrl = getUrl(parentLayout.value());
				
				if (cacheUrl != null) {
					if (url == null) {
						url = cacheUrl;
					} else {
						url = cacheUrl + "/" + url;
					}
				}
			}
		}
		
		return url;
	}
}