package de.kaesdingeling.commons.ui.utils.components;

import java.util.Locale;
import java.util.function.Function;

import com.vaadin.flow.component.HasTheme;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.html.Image;
import com.vaadin.flow.component.orderedlayout.FlexComponent.Alignment;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.TextFieldVariant;
import com.vaadin.flow.data.renderer.ComponentRenderer;

import de.kaesdingeling.commons.ui.utils.UIUtils;
import de.kaesdingeling.commons.ui.utils.components.data.interfaces.ILanguageChangerComponent;

/**
 * 
 * @created 14.06.2020 - 19:14:32
 * @author KaesDingeling
 * @version 0.1
 */
public class LanguageChanger extends Select<Locale> implements HasTheme, ILanguageChangerComponent {
	private static final long serialVersionUID = 2253355698257450952L;
	
	private boolean updateSilent = false;

	/**
	 * 
	 * @param titleGetter
	 * @param imageGetter
	 */
	public LanguageChanger(Function<Locale, String> titleGetter, Function<Locale, String> imageGetter) {
		super();
		
		addThemeName(TextFieldVariant.LUMO_SMALL.getVariantName());
		
		setRenderer(new ComponentRenderer<HorizontalLayout, Locale>(i -> {
			HorizontalLayout layout = new HorizontalLayout();
			
			Image image = new Image(imageGetter.apply(i), "Logo");
			image.getStyle().set("padding-right", "10px");
			image.getStyle().set("padding-left", "3px");
			image.setMaxHeight("13px");
			
			layout.getStyle().set("padding-bottom", "1px");
			layout.setAlignItems(Alignment.CENTER);
			layout.add(image);
			layout.add(titleGetter.apply(i));
			
			return layout;
		}));
		
		addValueChangeListener(e -> {
			if (!updateSilent) {
				UIUtils.updateLanguageForAllSessionUIs(UI.getCurrent(), e.getValue());
			}
		});
	}
	
	/**
	 * 
	 * 
	 * @Created 14.06.2020 - 19:19:23
	 * @author KaesDingeling
	 */
	@Override
	public void refresh() {
		UI ui = UI.getCurrent();
		
		setValueSilent(ui, ui.getLocale());
	}
	
	/**
	 * 
	 * @param locale
	 * @Created 14.06.2020 - 19:24:13
	 * @author KaesDingeling
	 */
	@Override
	public void setValueSilent(Locale locale) {
		setValueSilent(UI.getCurrent(), locale);
	}
	
	/**
	 * 
	 * @param ui
	 * @param locale
	 * @Created 14.06.2020 - 19:25:10
	 * @author KaesDingeling
	 */
	@Override
	public void setValueSilent(UI ui, Locale locale) {
		updateSilent = true;
		
		setValue(locale);
		
		updateSilent = false;
	}
}