package de.kaesdingeling.commons.ui.utils;

public class Styles {
	
	public static class PanelLayout {
		public static final String layout = "panelLayout";
	}
	
	public static class Dialog {
		public static final String confirm = "confirmDialog";
		public static final String layout = "dialogLayout";
	}
}