package de.kaesdingeling.commons.ui.utils.constants;

/**
 * 
 * @created 14.06.2020 - 19:00:56
 * @author KaesDingeling
 * @version 0.1
 */
public class KD_UI_CONSTANTS {

	public static final String THEME = "theme";
	public static final String THEME_CHECK = "theme-check";
	public static final String LANGUAGE = "language";
}