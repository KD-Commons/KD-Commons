package de.kaesdingeling.commons.ui.utils.components;

import com.vaadin.flow.component.HasValue;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.Binder.BindingBuilder;

import de.kaesdingeling.commons.ui.utils.interfaces.IBinderDialog;
import de.kaesdingeling.commons.ui.utils.theme.VaadinTheme;
import de.kaesdingeling.commons.utils.interfaces.EventListener;
import de.kaesdingeling.commons.utils.interfaces.TypedInputListener;
import de.kaesdingeling.commons.utils.interfaces.TypedOutputListener;
import lombok.Setter;

@SuppressWarnings("unchecked")
public abstract class BinderDialog<T, W extends BinderDialog<T, W>> extends Dialog implements IBinderDialog {
	private static final long serialVersionUID = -1808294654532385661L;
	
	protected Binder<T> binder = new Binder<T>();
	protected boolean changesSaved = true;
	
	protected Button save = null;
	protected Button remove = null;
	
	@Setter
	protected TypedInputListener<T> inputListener;
	@Setter
	protected TypedOutputListener<T> saveListener;
	@Setter
	protected TypedOutputListener<T> removeListener;
	@Setter
	protected EventListener closeListener;
	
	public W withInputListener(TypedInputListener<T> inputListener) {
		this.inputListener = inputListener;
		return (W) this;
	}
	
	public W withSaveListener(TypedOutputListener<T> saveListener) {
		this.saveListener = saveListener;
		return (W) this;
	}
	
	public W withRemoveListener(TypedOutputListener<T> removeListener) {
		this.removeListener = removeListener;
		return (W) this;
	}
	
	public W withCloseListener(EventListener closeListener) {
		this.closeListener = closeListener;
		return (W) this;
	}
	
	public void setBean(T t) {
		binder.setBean(t);
	}
	
	public T getBean() {
		return binder.getBean();
	}
	
	@Override
	public void open() {
		if (inputListener != null) {
			setBean(inputListener.event());
		}
		if (changesSaved) {
			binder.addValueChangeListener(e -> {
				saveValid(save, binder);
				if (changesSaved) {
					changesSaved = false;
				}
			});
		}
		if (remove != null && binder.getBean() != null) {
			remove.setVisible(false);
		}
		saveValid(save, binder);
		super.open();
	}
	
	protected void setSave(Button save) {
		save.addClickListener(e -> {
			if (binder.isValid()) {
				if (saveListener != null) {
					saveListener.event(binder.getBean());
				}
				save();
			}
		});
		this.save = save;
	}
	
	protected void setRemove(Button remove) {
		remove.addClickListener(e -> delete());
		VaadinTheme.ERROR.set(remove);
		this.remove = remove;
	}
	
	public void overrideClose() {
		changesSaved = true;
		close();
	}
	
	public Binder<T> getBinder() {
		return binder;
	}
	
	@Override
	public void close() {
		if (!changesSaved) {
			new ConfirmDialog("Änderungen verwerfen?", "Möchten Sie die Änderungen verwerfen?", "Ja", e -> overrideClose(), "Nein", e -> {
				
			}).open();
			return;
		}
		if (closeListener != null) {
			closeListener.event();
		}
		super.close();
	}
	
	protected <FIELDVALUE> BindingBuilder<T, FIELDVALUE> bind(HasValue<?, FIELDVALUE> field) {
		return binder.forField(field);
	}
}