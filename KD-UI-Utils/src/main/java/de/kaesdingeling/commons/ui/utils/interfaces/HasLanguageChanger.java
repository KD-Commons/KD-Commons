package de.kaesdingeling.commons.ui.utils.interfaces;

import de.kaesdingeling.commons.ui.utils.components.data.interfaces.ILanguageChangerComponent;

/**
 * 
 * @created 14.06.2020 - 18:49:19
 * @author KaesDingeling
 * @version 0.1
 */
public interface HasLanguageChanger {
	
	/**
	 * 
	 * @param <LC>
	 * @return
	 * @Created 18.06.2020 - 16:38:37
	 * @author KaesDingeling
	 */
	public <LC extends ILanguageChangerComponent> LC getLanguageChanger();
}