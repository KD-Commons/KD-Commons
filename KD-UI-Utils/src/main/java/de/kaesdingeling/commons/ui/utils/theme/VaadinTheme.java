package de.kaesdingeling.commons.ui.utils.theme;

import com.vaadin.flow.component.Component;

public enum VaadinTheme {
	PRIMARY,
	ERROR,
	SUCCESS,
	NONE;
	
	public void set(Component component) {
		String theme = "";
		
		if (!VaadinTheme.NONE.equals(this)) {
			theme = this.themeWithPrimary();
		}
		
		component.getElement().setAttribute("theme", theme);
	}
	
	public void setWithoutPrimary(Component component) {
		String theme = "";
		
		if (!VaadinTheme.NONE.equals(this)) {
			theme = this.theme();
		}
		
		component.getElement().setAttribute("theme", theme);
	}
	
	public String themeWithPrimary() {
		String theme = VaadinTheme.PRIMARY.theme();
		
		if (!VaadinTheme.PRIMARY.equals(this)) {
			theme += " " + this.theme();
		}
		
		return theme;
	}
	
	public String theme() {
		return this.name().toLowerCase();
	}
}